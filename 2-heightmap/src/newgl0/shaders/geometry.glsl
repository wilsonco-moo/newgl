#version 440 core

// Create a 4 vertex triangle strip for each input point.
layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

// Inputs from vertex shader. Note that there is only ever one, since
// we run for every POINT.
in float vgTopLeftColour[];

in vec4 vgTopRightCoord[];
in float vgTopRightColour[];

in vec4 vgBottomLeftCoord[];
in float vgBottomLeftColour[];

in vec4 vgBottomRightCoord[];
in float vgBottomRightColour[];

// Outputs to the fragment shader.
// Colour
out float gfColour;
// Texture coord
out vec2 gfTexCoord;


// The world size.
uniform int worldSize;

// How many grid cells the textures are spread across.
#define TEXTURE_SPREAD 24
#define TEX_AREA (1.0f / float(TEXTURE_SPREAD))

void main(void) {
    // Do nothing for the last vertex on each row
    if ((gl_PrimitiveIDIn + 1) % worldSize != 0) {
        
        // Get coordinate within world from vertex id.
        int x = gl_PrimitiveIDIn % worldSize,
            y = gl_PrimitiveIDIn / worldSize;
        
        // Find position within texture.
        float xOff = float(x % TEXTURE_SPREAD) / float(TEXTURE_SPREAD),
              yOff = float(y % TEXTURE_SPREAD) / float(TEXTURE_SPREAD);
        
        // Top left.
        gfColour = vgTopLeftColour[0];
        gl_Position = gl_in[0].gl_Position;
        gfTexCoord = vec2(xOff, yOff);
        EmitVertex();
        
        // Top right.
        gfColour = vgTopRightColour[0];
        gl_Position = vgTopRightCoord[0];
        gfTexCoord = vec2(xOff + TEX_AREA, yOff);
        EmitVertex();
        
        // Bottom left.
        gfColour = vgBottomLeftColour[0];
        gl_Position = vgBottomLeftCoord[0];
        gfTexCoord = vec2(xOff, yOff + TEX_AREA);
        EmitVertex();
        
        // Bottom right.
        gfColour = vgBottomRightColour[0];
        gl_Position = vgBottomRightCoord[0];
        gfTexCoord = vec2(xOff + TEX_AREA, yOff + TEX_AREA);
        EmitVertex();
    }
}
