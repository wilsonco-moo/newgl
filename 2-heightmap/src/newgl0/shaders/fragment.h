R"GLSL_END_TOKEN(#version 440 core

// Inputs from the geometry shader.
in float gfColour;
in vec2 gfTexCoord;

// Output colour.
out vec4 outColour;

// Texture input
layout (binding = 0) uniform sampler2D groundTex;
layout (binding = 1) uniform sampler2D pebblesTex;

void main(void) {
    outColour = mix(
        texture(groundTex, gfTexCoord),
        texture(pebblesTex, gfTexCoord),
        gfColour
    );
}
)GLSL_END_TOKEN"
