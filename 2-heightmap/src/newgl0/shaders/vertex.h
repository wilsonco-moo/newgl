R"GLSL_END_TOKEN(#version 440 core

// The 4 pairs of heights and colours, (inputs).
layout (location = 0) in float inTopLeftHeight;
layout (location = 1) in float inTopLeftColour;

layout (location = 2) in float inTopRightHeight;
layout (location = 3) in float inTopRightColour;

layout (location = 4) in float inBottomLeftHeight;
layout (location = 5) in float inBottomLeftColour;

layout (location = 6) in float inBottomRightHeight;
layout (location = 7) in float inBottomRightColour;

// Values to pass to geometry shader. Top left is passed as gl_Position.
out float vgTopLeftColour;

out vec4 vgTopRightCoord;
out float vgTopRightColour;

out vec4 vgBottomLeftCoord;
out float vgBottomLeftColour;

out vec4 vgBottomRightCoord;
out float vgBottomRightColour;

// The model-view-projection transformation matrix for all coordinates.
uniform mat4 mvpTransformMatrix;

// The world size.
uniform int worldSize;

// Maximum height
#define HEIGHT_MULTIPLIER 32.0f

void main(void) {
    
    // Get coordinate within world from vertex id.
    float x = float(gl_VertexID % worldSize),
          y = float(gl_VertexID / worldSize);
    
    // Work out x,y,z for each of 4 coords, then do transform.
    gl_Position = mvpTransformMatrix * vec4(x, y, inTopLeftHeight * HEIGHT_MULTIPLIER, 1.0f);
    vgTopLeftColour = inTopLeftColour;
    
    vgTopRightCoord = mvpTransformMatrix * vec4(x + 1, y, inTopRightHeight * HEIGHT_MULTIPLIER, 1.0f);
    vgTopRightColour = inTopRightColour;
    
    vgBottomLeftCoord = mvpTransformMatrix * vec4(x, y + 1, inBottomLeftHeight * HEIGHT_MULTIPLIER, 1.0f);
    vgBottomLeftColour = inBottomLeftColour;
    
    vgBottomRightCoord = mvpTransformMatrix * vec4(x + 1, y + 1, inBottomRightHeight * HEIGHT_MULTIPLIER, 1.0f);
    vgBottomRightColour = inBottomRightColour;
}
)GLSL_END_TOKEN"
