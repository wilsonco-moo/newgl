#include <GL/gl3w.h> // Include first

#include "MainRoom.h"

#include <wool/window/base/WindowBase.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/freeglut.h>
#include <glm/vec3.hpp>
#include <iostream>
#include <cstdlib>
#include <GL/gl.h>

namespace newgl {
    
    MainRoom::MainRoom(void) :
        vao(0),
        heightColourBuffer(0),
        projectionTransform(),
        modelViewTransform(),
        shaderProgram(0),
        worldSize(0),
        mvpTransformMatrixUniform(0),
        worldSizeUniform(0),
        ground(),
        pebbles(),
        timeStep(0.0f) {
    }

    MainRoom::~MainRoom(void) {
        
    }
    
    void MainRoom::compileAndCheckShader(const char * name, GLuint shader) {
        // Try to compile the shader.
        glCompileShader(shader);
        
        // If there is a log, print it on stderr.
        GLint len;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &len);
        if (len != 0) {
            std::string str(len, ' ');
            glGetShaderInfoLog(shader, len, NULL, &str[0]);
            std::cerr << str;
        }
        
        // If it didn't compile, complain.
        GLint isCompiled = 0;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
        if (isCompiled == GL_FALSE) {
            std::cerr << "ERROR: Failed to compile " << name << ".\n";
            exit(EXIT_FAILURE);
        }
    }
    
    void MainRoom::linkAndCheckProgram(const char * name, GLuint program) {
        // Try to link the shader.
        glLinkProgram(program);
        
        // If there is a log, print it on stderr.
        GLint len;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &len);
        if (len != 0) {
            std::string str(len, ' ');
            glGetProgramInfoLog(program, len, NULL, &str[0]);
            std::cerr << str;
        }
        
        // If it didn't link, complain.
        GLint isLinked = 0;
        glGetProgramiv(program, GL_LINK_STATUS, &isLinked);
        if (isLinked == GL_FALSE) {
            std::cerr << "ERROR: Failed to link " << name << ".\n";
            exit(EXIT_FAILURE);
        }
    }

    void MainRoom::init(void) {
        initGl3w();
        compileShaders();
        setupUniforms();
        loadTexturesAndBuffers();
        // Misc initialisation
        glEnable(GL_DEPTH_TEST);
    }
    
    void MainRoom::initGl3w(void) {
        if (gl3wInit()) {
            std::cerr << "Failed to initialize gl3w.\n";
            exit(EXIT_FAILURE);
        }
        if (!gl3wIsSupported(4, 4)) {
            std::cerr << "OpenGL 4.4 not supported\n";
            exit(EXIT_FAILURE);
        }
    }    
    
    void MainRoom::compileShaders(void) {
        
        // String literals for shader sources.
        const char * vertexShaderSource = 
        #include "shaders/vertex.h"
        ;
        const char * geometryShaderSource = 
        #include "shaders/geometry.h"
        ;
        const char * fragmentShaderSource = 
        #include "shaders/fragment.h"
        ;
        
        // Create and compile vertex shader.
        GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
        compileAndCheckShader("vertex shader", vertexShader);
        
        // Create and compile geometry shader.
        GLuint geometryShader = glCreateShader(GL_GEOMETRY_SHADER);
        glShaderSource(geometryShader, 1, &geometryShaderSource, NULL);
        compileAndCheckShader("geometry shader", geometryShader);
        
        // Create and compile fragment shader.
        GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
        compileAndCheckShader("fragment shader", fragmentShader);
        
        // Create shader program, attach both shaders and link it.
        shaderProgram = glCreateProgram();
        glAttachShader(shaderProgram, vertexShader);
        glAttachShader(shaderProgram, geometryShader);
        glAttachShader(shaderProgram, fragmentShader);
        linkAndCheckProgram("main shader program", shaderProgram);
        
        // Delete the compiled shaders: We don't need them any more since they
        // are part of the program.
        glDeleteShader(vertexShader);
        glDeleteShader(geometryShader);
        glDeleteShader(fragmentShader);
    }
    
    void MainRoom::setupUniforms(void) {
        mvpTransformMatrixUniform = glGetUniformLocation(shaderProgram, "mvpTransformMatrix");
        worldSizeUniform = glGetUniformLocation(shaderProgram, "worldSize");
    }

    void MainRoom::loadTexturesAndBuffers(void) {
        // Load all textures from files, complain if any failed to load.
        ground.load("assets/ground.png", false, true, GL_LINEAR, GL_LINEAR);
        pebbles.load("assets/pebbles.png", false, true, GL_LINEAR, GL_LINEAR);
        glBindTextureUnit(0, ground.getTextureId());
        glBindTextureUnit(1, pebbles.getTextureId());
        {
            wool::Texture colourMap("assets/colourMap.png", true, false),
                          heightMap("assets/heightMap.png", true, false);
            if (!ground.isLoaded() || !pebbles.isLoaded() || !colourMap.isLoaded() || !heightMap.isLoaded()) {
                std::cerr << "One or more textures failed to load.\n";
                exit(EXIT_FAILURE);
            }
            
            // Find the world size, and tell it to the shaders: The size of the heightmap and colour map textures. If either of the textures
            // are non-square, or are different sizes, complain.
            worldSize = (GLuint)colourMap.getWidth();
            glUniform1i(worldSizeUniform, worldSize);
            if ((GLuint)colourMap.getHeight() != worldSize || (GLuint)heightMap.getWidth() != worldSize || (GLuint)heightMap.getHeight() != worldSize) {
                std::cerr << "One or more of the colour map or height map textures are not square, or are different sizes.\n";
                exit(EXIT_FAILURE);
            }
            
            // Make sure the size of HeightColourPair is actually 2. This should never happen (at least not on an x86).
            if (sizeof(HeightColourPair) != 2) {
                std::cerr << "sizeof(HeightColourPair) is wrong: " << sizeof(HeightColourPair) << "\n";
                exit(EXIT_FAILURE);
            }
            
            // Create ourself a buffer, able to store enough HeightColourPairs, able to be mapped for writing, and map it.
            // An extra HeightColourPair is required, but is never used, due to the coordinates.
            glCreateBuffers(1, &heightColourBuffer);
            glNamedBufferStorage(heightColourBuffer, (worldSize * worldSize + 1) * sizeof(HeightColourPair), NULL, GL_MAP_WRITE_BIT);
            void * mappedBuffer = glMapNamedBuffer(heightColourBuffer, GL_WRITE_ONLY);
            HeightColourPair * mappedBufferUpTo = (HeightColourPair *)mappedBuffer;
            
            // Fill the mapped buffer with appropriate values for colour and height, from the colourMap and heightMap textures.
            for (GLuint iy = 0; iy < worldSize; iy++) {
                for (GLuint ix = 0; ix < worldSize; ix++) {
                    uint32_t col = colourMap.getPixelInt(ix, iy);
                    mappedBufferUpTo->colour = (GLubyte)((col >> 24) & 255);
                    
                    col = heightMap.getPixelInt(ix, iy);
                    mappedBufferUpTo->height = (GLubyte)((col >> 24) & 255);
                    
                    mappedBufferUpTo++;
                }
            }
            
            glUnmapNamedBuffer(heightColourBuffer);
        }
        
        // Create a vertex array object.
        glCreateVertexArrays(1, &vao);
        
        // Bind each attribute to buffer binding 0, and a 2*float size.
        // Top left
        glVertexArrayAttribBinding(vao, 0, 0);
        glVertexArrayAttribFormat(vao, 0, 1, GL_UNSIGNED_BYTE, GL_TRUE, offsetof(HeightColourPair, height));
        glVertexArrayAttribBinding(vao, 1, 0);
        glVertexArrayAttribFormat(vao, 1, 1, GL_UNSIGNED_BYTE, GL_TRUE, offsetof(HeightColourPair, colour));
        // Top right
        glVertexArrayAttribBinding(vao, 2, 0);
        glVertexArrayAttribFormat(vao, 2, 1, GL_UNSIGNED_BYTE, GL_TRUE, offsetof(HeightColourPair, height) + sizeof(HeightColourPair));
        glVertexArrayAttribBinding(vao, 3, 0);
        glVertexArrayAttribFormat(vao, 3, 1, GL_UNSIGNED_BYTE, GL_TRUE, offsetof(HeightColourPair, colour) + sizeof(HeightColourPair));
        // Bottom left
        glVertexArrayAttribBinding(vao, 4, 0);
        glVertexArrayAttribFormat(vao, 4, 1, GL_UNSIGNED_BYTE, GL_TRUE, offsetof(HeightColourPair, height) + worldSize * sizeof(HeightColourPair));
        glVertexArrayAttribBinding(vao, 5, 0);
        glVertexArrayAttribFormat(vao, 5, 1, GL_UNSIGNED_BYTE, GL_TRUE, offsetof(HeightColourPair, colour) + worldSize * sizeof(HeightColourPair));
        // Bottom right
        glVertexArrayAttribBinding(vao, 6, 0);
        glVertexArrayAttribFormat(vao, 6, 1, GL_UNSIGNED_BYTE, GL_TRUE, offsetof(HeightColourPair, height) + (worldSize + 1) * sizeof(HeightColourPair));
        glVertexArrayAttribBinding(vao, 7, 0);
        glVertexArrayAttribFormat(vao, 7, 1, GL_UNSIGNED_BYTE, GL_TRUE, offsetof(HeightColourPair, colour) + (worldSize + 1) * sizeof(HeightColourPair));
        
        // Bind the buffer to binding index zero within the vertex array object. Stride is size of HeightColourPair.
        glVertexArrayVertexBuffer(vao, 0, heightColourBuffer, 0, sizeof(HeightColourPair));
        
        // Enable all attributes (enable automatic fetching).
        glEnableVertexArrayAttrib(vao, 0);
        glEnableVertexArrayAttrib(vao, 1);
        glEnableVertexArrayAttrib(vao, 2);
        glEnableVertexArrayAttrib(vao, 3);
        glEnableVertexArrayAttrib(vao, 4);
        glEnableVertexArrayAttrib(vao, 5);
        glEnableVertexArrayAttrib(vao, 6);
        glEnableVertexArrayAttrib(vao, 7);
        
        // Bind the vertex array and buffer.
        glBindVertexArray(vao);
        glBindBuffer(GL_ARRAY_BUFFER, heightColourBuffer);
    }
    
    void MainRoom::step(void) {
        
        // Clear colour buffer with colour, clear depth and stencil buffers.
        GLfloat colour[4] = { 0.56f, 0.92f, 1.0f, 1.0f };
        glClearBufferfv(GL_COLOR, 0, colour);
        glClearBufferfi(GL_DEPTH_STENCIL, 0, 1.0f, 0);
        
        // Use the shader program.
        glUseProgram(shaderProgram);
        
        // Update the look matrix.
        timeStep += 0.3f * getWindowBase()->elapsed();
        timeStep = fmod(timeStep, 2.0f * M_PI);
        modelViewTransform = glm::lookAt(
            glm::vec3(128 * cos(timeStep) + 128, 128 * -sin(timeStep) + 128, 50),
            glm::vec3(128,128,0), 
            glm::vec3(0, 0, 1)
        );
        
        // Tell the shader about the new model view projection matrix.
        glm::mat4 mvpTransform = projectionTransform * modelViewTransform;
        glUniformMatrix4fv(mvpTransformMatrixUniform, 1, GL_FALSE, glm::value_ptr(mvpTransform));
        
        // Tell the shader about the current world size.
        glUniform1i(worldSizeUniform, worldSize);
        
        // Draw an appropriate number of points. That is one for each row, except
        // the last row, except the last cell.
        glDrawArrays(GL_POINTS, 0, ((worldSize - 1) * worldSize) - 1);
        
        glutSwapBuffers();
    }
    
    void MainRoom::reshape(GLsizei width, GLsizei height) {
        glViewport(0.0f, 0.0f, width, height);
        GLfloat aspect = ((GLfloat)width) / ((GLfloat)height);
        // 50 degrees fovy
        projectionTransform = glm::perspective(0.872664626f, aspect, 0.1f, 1000.0f);
    }
    
    void MainRoom::end(void) {
    }
    void MainRoom::keyNormal(unsigned char key, int x, int y) {}
    void MainRoom::keyNormalRelease(unsigned char key, int x, int y) {}
    void MainRoom::keySpecial(int key, int x, int y) {}
    void MainRoom::keySpecialRelease(int key, int x, int y) {}
    void MainRoom::mouseEvent(int button, int state, int x, int y) {}
    void MainRoom::mouseMove(int x, int y) {}
    void MainRoom::mouseDrag(int x, int y) {}
    bool MainRoom::shouldDeleteOnRoomEnd(void) { return false; }

}
