#ifndef NEWGL_MAINROOM_H_
#define NEWGL_MAINROOM_H_

#include <wool/room/base/RoomBase.h>
#include <wool/texture/Texture.h>
#include <glm/mat4x4.hpp>
#include <GL/gl.h>
#include <array>

namespace newgl {

    class MainRoom : public wool::RoomBase {
    private:
        /**
         * This should be used for passing to shaders pairs of heights and
         * colours. A load of these will be stored in the buffer.
         */
        class HeightColourPair {
        public:
            GLubyte height, colour;
        };
    
        // The vertex array object for feeding data to the shader, from the buffer.
        GLuint vao;
        
        // The buffer in which we can store the pairs of height and colour.
        GLuint heightColourBuffer;
        
        // The projection matrix, (view space to clip space).
        glm::mat4 projectionTransform;
        
        // The model-view matrix, (world (model) space to view space).
        glm::mat4 modelViewTransform;
        
        // Our shader program. This is set up in compileShaders.
        GLuint shaderProgram;
        
        // The size of the world.
        GLuint worldSize;
        
        // Uniform locations for matrices and texture size.
        GLuint mvpTransformMatrixUniform,
               worldSizeUniform;
        
        // Textures for drawing.
        wool::Texture ground, pebbles;
        
        // For camera movement.
        GLfloat timeStep;
        
    public:
        MainRoom(void);
        virtual ~MainRoom(void);
    private:
        static void compileAndCheckShader(const char * name, GLuint shader);
        static void linkAndCheckProgram(const char * name, GLuint program);
    
        void initGl3w(void);
        void compileShaders(void);
        void setupUniforms(void);
        void loadTexturesAndBuffers(void);
        
    public:
        virtual void init(void) override;
        virtual void step(void) override;
        virtual void end(void) override;
        virtual void keyNormal(unsigned char key, int x, int y) override;
        virtual void keyNormalRelease(unsigned char key, int x, int y) override;
        virtual void keySpecial(int key, int x, int y) override;
        virtual void keySpecialRelease(int key, int x, int y) override;
        virtual void reshape(GLsizei width, GLsizei height) override;
        virtual void mouseEvent(int button, int state, int x, int y) override;
        virtual void mouseMove(int x, int y) override;
        virtual void mouseDrag(int x, int y) override;
        virtual bool shouldDeleteOnRoomEnd(void) override;

    };
}

#endif
