/*
 * SkelNode.cpp
 *
 *  Created on: 6 Jan 2020
 *      Author: wilson
 */

#include "SkelNode.h"

#include <glm/vec4.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace newgl {

    SkelNode::SkelNode(glm::mat4 baseTransform) :
        baseTransform(baseTransform),
        calcPos(),
        calcTransform(),
        children() {
    }
    
    void SkelNode::updateCalc(const glm::mat4 & transformWithin) {
        calcTransform = transformWithin * baseTransform;
        calcPos = glm::vec3(calcTransform * glm::vec4(0,0,0,1));
        for (SkelNode * child : children) {
            child->updateCalc(calcTransform);
        }
    }
    
    void SkelNode::addChild(SkelNode * child) {
        children.push_back(child);
    }
}
