/*
 * SkelNode.h
 *
 *  Created on: 6 Jan 2020
 *      Author: wilson
 */

#ifndef NEWGL0_SKELNODE_H_
#define NEWGL0_SKELNODE_H_

#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include <vector>

namespace newgl {

    /**
     *
     */
    class SkelNode {
    private:
        glm::mat4 baseTransform;
        
        glm::vec3 calcPos;
        glm::mat4 calcTransform;
        
        std::vector<SkelNode *> children;
    
    public:
        SkelNode(glm::mat4 baseTransform);
        
        void updateCalc(const glm::mat4 & transformWithin = glm::mat4(1.0f));
        void addChild(SkelNode * node);
        
        inline const glm::vec3 & getCalcPos(void) const {
            return calcPos;
        }
        inline const glm::mat4 & getCalcTransform(void) const {
            return calcTransform;
        }
        
        inline void setBaseTransform(const glm::mat4 & transform) {
            baseTransform = transform;
        }
        
    };
}

#endif
