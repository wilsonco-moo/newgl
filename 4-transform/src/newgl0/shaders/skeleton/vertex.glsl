#version 140

// Inputs (vertex attributes).
in vec3 inPosition;

// Outputs (to fragment shader).
out vec4 vfColour;

// Uniforms
uniform mat4 mvpTransformMatrix;

void main(void) {
    gl_Position = mvpTransformMatrix * vec4(inPosition.xyz, 1.0f);
    vfColour = vec4(gl_VertexID % 2, gl_VertexID % 32 > 15 ? 1.0f : 0.0f, 1.0f, 1.0f);
}
