#version 140

// Inputs (from vertex shader)
in vec4 vfColour;

// Outputs
out vec4 outColour;

void main(void) {
    outColour = vfColour;
}
