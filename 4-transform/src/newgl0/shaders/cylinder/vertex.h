R"GLSL_END_TOKEN(#version 140

// Inputs (vertex attributes).
in uint inPoint1;
in uint inPoint2;
in vec3 inPosition1;
in vec3 inPosition2;
in float inMix;

// Outputs (to fragment shader).
out vec4 vfColour;

// Uniforms
uniform mat4 mvpTransformMatrix;
uniform mat4 pointTransform[5];

void main(void) {
    
    vec4 pos1 = pointTransform[inPoint1] * vec4(inPosition1, 1.0f);
    vec4 pos2 = pointTransform[inPoint2] * vec4(inPosition2, 1.0f);
    gl_Position = mvpTransformMatrix * mix(pos1, pos2, inMix);
    
    vfColour = vec4(gl_VertexID % 2, gl_VertexID % 64 > 31 ? 1.0f : 0.0f, 1.0f, 1.0f);
}
)GLSL_END_TOKEN"
