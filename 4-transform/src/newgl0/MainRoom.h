#ifndef NEWGL_MAINROOM_H_
#define NEWGL_MAINROOM_H_

#include <wool/room/base/RoomBase.h>
#include <wool/texture/Texture.h>
#include <glm/mat4x4.hpp>
#include <unordered_map>
#include <glm/vec3.hpp>
#include <GL/gl.h>
#include <vector>
#include <string>
#include <array>

#include "SkelNode.h"

namespace newgl {

    class MainRoom : public wool::RoomBase {
    private:
        
        // This contains all the data required for each vertex.
        class VertexInfo {
        public:
            GLuint point1, point2;
            glm::vec3 position1, position2;
            GLfloat mix;
        };
        
        // Make sure the compiler hasn't done anything funny with the layout.
        static_assert(
            sizeof(VertexInfo) == sizeof(GLfloat) * 6 + sizeof(GLuint) * 2  + sizeof(GLfloat) &&
            offsetof(VertexInfo, point1) == 0 &&
            offsetof(VertexInfo, point2) == 4 &&
            offsetof(VertexInfo, position1) == 8 &&
            offsetof(VertexInfo, position2) == 20 &&
            offsetof(VertexInfo, mix) == 32
        );
        
        // The vertex array object for feeding data to the shader, from the buffer.
        GLuint vao;
        
        // The vertex array object for skeletons.
        GLuint skeletonVao;
        
        // Buffers for vertices.
        GLuint vertexBuffer, indexBuffer;
        
        // Buffer for the drawing of the skeleton.
        GLuint skeletonBuffer;
        
        // Projection and model-view transforms.
        glm::mat4 projectionTransform, modelViewTransform;
        
        // Our shader programs. These are set up in compileShaders.
        GLuint cylinderShader, skeletonShader;
        
        // The number of vertices, and vertex indices.
        GLsizei vertexCount,
                indexCount;
                
        // The number of skeleton vertices.
        GLsizei skeletonCount;
        
        // Uniform locations for matrices and texture size.
        GLuint mvpUniformCylinder, mvpUniformSkeleton, pointTransformUniform;
        
        // For camera movement.
        GLfloat timeStep;
        
        // For terrain height adjustment.
        GLfloat totalTimeElapsed;
        
        // Skel nodes.
        std::vector<SkelNode> skelNodes;
        
    public:
        MainRoom(void);
        virtual ~MainRoom(void);
    private:
        static void compileAndCheckShader(const std::string & name, GLuint shader);
        static void linkAndCheckProgram(const std::string & name, GLuint program);
        static GLuint compileShaderProgram(const std::string & name, const char * vertexShaderSource, const char * geometryShaderSource, const char * fragmentShaderSource);
    
        void initGl3w(void);
        void compileShaders(void);
        void setupUniforms(void);
        void loadVertices(void);
        void setupVertexArray(void);
        
        // Skeleton stuff
        void setupSkeleton(void);
        MainRoom::VertexInfo coordinateIntoSkeleton(glm::vec3 coord);
        void updateAndDrawSkeleton(void);
        
    public:
        virtual void init(void) override;
        virtual void step(void) override;
        virtual void end(void) override;
        virtual void keyNormal(unsigned char key, int x, int y) override;
        virtual void keyNormalRelease(unsigned char key, int x, int y) override;
        virtual void keySpecial(int key, int x, int y) override;
        virtual void keySpecialRelease(int key, int x, int y) override;
        virtual void reshape(GLsizei width, GLsizei height) override;
        virtual void mouseEvent(int button, int state, int x, int y) override;
        virtual void mouseMove(int x, int y) override;
        virtual void mouseDrag(int x, int y) override;
        virtual bool shouldDeleteOnRoomEnd(void) override;

    };
}

#endif
