#include <GL/gl3w.h> // Include first

#include "MainRoom.h"

#include <wool/texture/lodepng/lodepng.h>
#include <wool/window/base/WindowBase.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/freeglut.h>
#include <iostream>
#include <cstdlib>
#include <GL/gl.h>

#define CYL_X 0.0f
#define CYL_Y -3.5f
#define CYL_Z 0.0f
#define CYL_LEN 8.0f
#define CYL_RADIUS 0.75f
#define CYL_RINGS 32
#define CYL_SEGMENTS 40

#define SKELETON_DISTANCE ((GLfloat)(((CYL_RINGS - 1.0f) / CYL_RINGS) * CYL_LEN) / (skeletonCount - 1))

namespace newgl {
    
    MainRoom::MainRoom(void) :
        vao(0),
        skeletonVao(0),
        vertexBuffer(0),
        indexBuffer(0),
        skeletonBuffer(0),
        projectionTransform(),
        modelViewTransform(),
        cylinderShader(0),
        skeletonShader(0),
        vertexCount(0),
        indexCount(0),
        skeletonCount(0),
        mvpUniformCylinder(0),
        mvpUniformSkeleton(0),
        pointTransformUniform(0),
        timeStep(0.0f),
        totalTimeElapsed(0.0f),
        skelNodes() {
    }

    MainRoom::~MainRoom(void) {
    }
    
    void MainRoom::compileAndCheckShader(const std::string & name, GLuint shader) {
        // Try to compile the shader.
        glCompileShader(shader);
        
        // If there is a log, print it on stderr.
        GLint len;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &len);
        if (len != 0) {
            std::string str(len, ' ');
            glGetShaderInfoLog(shader, len, NULL, &str[0]);
            std::cerr << str;
        }
        
        // If it didn't compile, complain.
        GLint isCompiled = 0;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
        if (isCompiled == GL_FALSE) {
            std::cerr << "ERROR: Failed to compile " << name << ".\n";
            exit(EXIT_FAILURE);
        }
    }
    
    void MainRoom::linkAndCheckProgram(const std::string & name, GLuint program) {
        // Try to link the shader.
        glLinkProgram(program);
        
        // If there is a log, print it on stderr.
        GLint len;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &len);
        if (len != 0) {
            std::string str(len, ' ');
            glGetProgramInfoLog(program, len, NULL, &str[0]);
            std::cerr << str;
        }
        
        // If it didn't link, complain.
        GLint isLinked = 0;
        glGetProgramiv(program, GL_LINK_STATUS, &isLinked);
        if (isLinked == GL_FALSE) {
            std::cerr << "ERROR: Failed to link " << name << ".\n";
            exit(EXIT_FAILURE);
        }
    }
    
    GLuint MainRoom::compileShaderProgram(const std::string & name, const char * vertexShaderSource, const char * geometryShaderSource, const char * fragmentShaderSource) {
        // Create and compile vertex shader.
        GLuint vertexShader, geometryShader, fragmentShader;
        
        // Create and compile vertex shader.
        if (vertexShaderSource != NULL) {
            vertexShader = glCreateShader(GL_VERTEX_SHADER);
            glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
            compileAndCheckShader(name+": vertex shader", vertexShader);
        }
        
        // Create and compile geometry shader.
        if (geometryShaderSource != NULL) {
            geometryShader = glCreateShader(GL_GEOMETRY_SHADER);
            glShaderSource(geometryShader, 1, &geometryShaderSource, NULL);
            compileAndCheckShader(name+": geometry shader", geometryShader);
        }
        
        // Create and compile fragment shader.
        if (fragmentShaderSource != NULL) {
            fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
            glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
            compileAndCheckShader("fragment shader", fragmentShader);
        }
        
        // Create shader program, attach all shaders and link it.
        GLuint prog = glCreateProgram();
        if (vertexShaderSource != NULL)   glAttachShader(prog, vertexShader);
        if (geometryShaderSource != NULL) glAttachShader(prog, geometryShader);
        if (fragmentShaderSource != NULL) glAttachShader(prog, fragmentShader);
        linkAndCheckProgram("main shader program", prog);
        
        // Delete the compiled shaders: We don't need them any more since they
        // are part of the program.
        if (vertexShaderSource != NULL)   glDeleteShader(vertexShader);
        if (geometryShaderSource != NULL) glDeleteShader(geometryShader);
        if (fragmentShaderSource != NULL) glDeleteShader(fragmentShader);
        
        return prog;
    }

    void MainRoom::init(void) {
        initGl3w();
        compileShaders();
        setupUniforms();
        setupSkeleton();
        loadVertices();
        setupVertexArray();
        // Misc initialisation
        glEnable(GL_DEPTH_TEST);
    }
    
    void MainRoom::initGl3w(void) {
        if (gl3wInit()) {
            std::cerr << "Failed to initialize gl3w.\n";
            exit(EXIT_FAILURE);
        }
        if (!gl3wIsSupported(3, 1)) {
            std::cerr << "OpenGL 3.1 not supported\n";
            exit(EXIT_FAILURE);
        }
    }
    
    void MainRoom::compileShaders(void) {
        std::cout << "Compiling shaders...\n";
        
        cylinderShader = compileShaderProgram(
            "Cylinder shader",
            #include "shaders/cylinder/vertex.h"
            , NULL,
            #include "shaders/cylinder/fragment.h"
        );
        
        skeletonShader = compileShaderProgram(
            "Cylinder shader",
            #include "shaders/skeleton/vertex.h"
            , NULL,
            #include "shaders/skeleton/fragment.h"
        );
        
        std::cout << "Done.\n";
    }
    
    void MainRoom::setupUniforms(void) {
        mvpUniformCylinder = glGetUniformLocation(cylinderShader, "mvpTransformMatrix");
        mvpUniformCylinder = glGetUniformLocation(skeletonShader, "mvpTransformMatrix");
        pointTransformUniform = glGetUniformLocation(cylinderShader, "pointTransform");
    }

    void MainRoom::loadVertices(void) {

        // Use first vertex convention.
        glProvokingVertex(GL_FIRST_VERTEX_CONVENTION);
        
        // Work out size of vertex and vertex index data.
        vertexCount = CYL_RINGS * CYL_SEGMENTS;
        indexCount = (2 * CYL_RINGS + 1) * CYL_SEGMENTS;
        
        // Work out the size of the data.
        GLsizei vertexDataSize = vertexCount * sizeof(VertexInfo);
        GLsizei indexDataSize = indexCount * sizeof(GLuint);
        
        // Set the restart index to one after the end, and enable primitive restart.
        GLuint restartIndex = indexDataSize;
        glPrimitiveRestartIndex(restartIndex);
        glEnable(GL_PRIMITIVE_RESTART);
        
        // Create and bind buffers for vertex data and vertex index data.
        glGenBuffers(1, &vertexBuffer);
        glGenBuffers(1, &indexBuffer);
        
        glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
        
        glBufferData(GL_ARRAY_BUFFER, vertexDataSize, NULL, GL_STATIC_READ);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexDataSize, NULL, GL_STATIC_READ);
        
        void * mappedVertexData = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY),
             * mappedIndexData = glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);
        
        // Cast appropriately so we can write to the buffer.
        VertexInfo * vertexDataWrite = (VertexInfo *)mappedVertexData;
        GLuint * indexDataWrite = (GLuint *)mappedIndexData;
        
        // Fill buffers.
        for (int segmentId = 0; segmentId < CYL_SEGMENTS; segmentId++) {
            GLfloat angle = ((GLfloat)segmentId / (GLfloat)CYL_SEGMENTS) * 2.0f * M_PI;
            
            for (int ringId = 0; ringId < CYL_RINGS; ringId++) {
                
                *vertexDataWrite = coordinateIntoSkeleton(glm::vec3(
                    CYL_X + CYL_RADIUS * cos(angle),
                    CYL_Y + ((GLfloat)ringId / (GLfloat)CYL_RINGS) * CYL_LEN,
                    CYL_Z + CYL_RADIUS * sin(angle)
                ));
                vertexDataWrite++;
                
                *(indexDataWrite++) = (segmentId * CYL_RINGS) + ringId;
                *(indexDataWrite++) = (((segmentId + 1) % CYL_SEGMENTS) * CYL_RINGS) + ringId;
                
                if (ringId == CYL_RINGS - 1) {
                    *(indexDataWrite++) = restartIndex;
                }
            }
        }
        
        // Unmap the buffers now we have finished writing to them
        glUnmapBuffer(GL_ARRAY_BUFFER);
        glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
        
        // These don't need to be bound any more.
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        std::cout << "Done.\n";
    }
    
    void MainRoom::setupVertexArray(void) {
        
        // Get the locations within the vertex shader of the inputs.
        GLuint point1Loc = glGetAttribLocation(cylinderShader, "inPoint1"),
               point2Loc = glGetAttribLocation(cylinderShader, "inPoint2"),
               position1Loc = glGetAttribLocation(cylinderShader, "inPosition1"),
               position2Loc = glGetAttribLocation(cylinderShader, "inPosition2"),
               mixLoc = glGetAttribLocation(cylinderShader, "inMix");
        
        // Create and bind a vertex array object.
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);
        
        // Bind the array buffer, so the glVertexAttribPointer calls know where to go.
        glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
        
        // Create each of the 4 attributes. Height and should come through as normalised floats, the other two
        // should come through just as integers.
        glVertexAttribIPointer(point1Loc,    1, GL_UNSIGNED_INT,    sizeof(VertexInfo), (void *)offsetof(VertexInfo, point1));
        glVertexAttribIPointer(point2Loc,    1, GL_UNSIGNED_INT,    sizeof(VertexInfo), (void *)offsetof(VertexInfo, point2));
        glVertexAttribPointer (position1Loc, 3, GL_FLOAT, GL_FALSE, sizeof(VertexInfo), (void *)offsetof(VertexInfo, position1));
        glVertexAttribPointer (position2Loc, 3, GL_FLOAT, GL_FALSE, sizeof(VertexInfo), (void *)offsetof(VertexInfo, position2));
        glVertexAttribPointer (mixLoc,       1, GL_FLOAT, GL_FALSE, sizeof(VertexInfo), (void *)offsetof(VertexInfo, mix));
        
        // This doesn't need to be bound any more.
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        // Enable each of the attributes.
        glEnableVertexAttribArray(point1Loc);
        glEnableVertexAttribArray(point2Loc);
        glEnableVertexAttribArray(position1Loc);
        glEnableVertexAttribArray(position2Loc);
        glEnableVertexAttribArray(mixLoc);
    }
    
    void MainRoom::setupSkeleton(void) {
        
        // Setup values.
        skeletonCount = 5;
        GLsizei skeletonDataSize = skeletonCount * sizeof(glm::vec3);
        
        // Create skeleton nodes and initialise them.
        for (int i = 0; i < skeletonCount; i++) {
            skelNodes.emplace_back(
                glm::translate(
                    glm::mat4(1.0f),
                    i == 0 ? glm::vec3(0, -3.5, 0) : glm::vec3(0, SKELETON_DISTANCE, 0)
                )
            );
        }
        for (int i = 0; i < skeletonCount - 1; i++) {
            skelNodes[i].addChild(&skelNodes[i + 1]);
        }
        skelNodes[0].updateCalc();
        
        // Create, bind and allocate buffer.
        glGenBuffers(1, &skeletonBuffer);
        glBindBuffer(GL_ARRAY_BUFFER, skeletonBuffer);
        glBufferData(GL_ARRAY_BUFFER, skeletonDataSize, NULL, GL_STATIC_READ);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        // Create a vertex array object and give it an attribute.
        GLuint positionLoc = glGetAttribLocation(skeletonShader, "inPosition");
        glGenVertexArrays(1, &skeletonVao);
        glBindVertexArray(skeletonVao);
        glBindBuffer(GL_ARRAY_BUFFER, skeletonBuffer);
        glVertexAttribPointer(positionLoc, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3), (void *)0);
        glEnableVertexAttribArray(positionLoc);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
    
    MainRoom::VertexInfo MainRoom::coordinateIntoSkeleton(glm::vec3 coord) {
        GLfloat closestDist = 10000000,
                secondClosestDist = 10000000;
        int closestId = 0,
            secondClosestId = 0;
        
        for (int i = 0; i < skeletonCount; i++) {
            GLfloat dist = glm::distance(coord, skelNodes[i].getCalcPos());
            if (dist < closestDist) {
                closestDist = dist;
                closestId = i;
            }
        }
        for (int i = 0; i < skeletonCount; i++) {
            GLfloat dist = glm::distance(coord, skelNodes[i].getCalcPos());
            if (i != closestId && dist < secondClosestDist) {
                secondClosestDist = dist;
                secondClosestId = i;
            }
        }
        
        VertexInfo v;
        
        v.point1 = closestId;
        v.point2 = secondClosestId;
        v.position1 = coord - skelNodes[closestId].getCalcPos();
        v.position2 = coord - skelNodes[secondClosestId].getCalcPos();
        v.mix = (closestDist - CYL_RADIUS) / (closestDist + secondClosestDist - CYL_RADIUS * 2.0f);
        return v;
    }

    void MainRoom::updateAndDrawSkeleton(void) {
        
        // Write data to buffer.
        glBindBuffer(GL_ARRAY_BUFFER, skeletonBuffer);
        void * mappedSkeletonData = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
        glm::vec3 * skeletonDataWrite = (glm::vec3 *)mappedSkeletonData;
        
        #define ROTATION_AMOUNT 0.4
        #define ROTATION_OFF1 0.0f
        #define ROTATION_OFF2 0.0f
        
        skelNodes[2].setBaseTransform(
            glm::rotate(
                glm::translate(
                    glm::rotate(
                        glm::mat4(1.0f),
                        (GLfloat)(sin(timeStep + ROTATION_OFF1) * ROTATION_AMOUNT),
                        glm::vec3(0, 0, 1)
                    ),
                    glm::vec3(0, SKELETON_DISTANCE, 0)
                ),
                (GLfloat)(sin(timeStep + ROTATION_OFF1) * ROTATION_AMOUNT),
                glm::vec3(0, 0, 1)
            )
        );
        
        skelNodes[3].setBaseTransform(
            glm::rotate(
                glm::translate(
                    glm::rotate(
                        glm::mat4(1.0f),
                        (GLfloat)(sin(timeStep + ROTATION_OFF2) * ROTATION_AMOUNT),
                        glm::vec3(0, 0, 1)
                    ),
                    glm::vec3(0, SKELETON_DISTANCE, 0)
                ),
                (GLfloat)(sin(timeStep + ROTATION_OFF2) * ROTATION_AMOUNT),
                glm::vec3(0, 0, 1)
            )
        );
        
        skelNodes[0].updateCalc();
        for (SkelNode & node : skelNodes) {
            *(skeletonDataWrite++) = node.getCalcPos();
        }
        glUnmapBuffer(GL_ARRAY_BUFFER);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        // Draw the buffer.
        glBindVertexArray(skeletonVao);
        glPointSize(8);
        glDrawArrays(GL_POINTS, 0, skeletonCount);
        glBindVertexArray(0);
    }
    
    void MainRoom::step(void) {
        
        // Clear colour buffer with colour, clear depth and stencil buffers.
        GLfloat colour[4] = { 0.56f, 0.92f, 1.0f, 1.0f };
        glClearBufferfv(GL_COLOR, 0, colour);
        glClearBufferfi(GL_DEPTH_STENCIL, 0, 1.0f, 0);
        
        
        //#define SPINNY
        
        // Update the look matrix.
        timeStep += 0.8f * getWindowBase()->elapsed();
        timeStep = fmod(timeStep, 8.0f * M_PI);
        #ifdef SPINNY
            modelViewTransform = glm::lookAt(
                glm::vec3(5 * cos(timeStep), 5 * -sin(timeStep), 5),
                glm::vec3(0, 0, 0), 
                glm::vec3(0, 0, 1)
            );
        #else
            modelViewTransform = glm::lookAt(
                glm::vec3(-5, -5, 5),
                glm::vec3(0, 0, 0), 
                glm::vec3(0, 0, 1)
            );
        #endif
        
        // Tell the shader about the new model view projection matrix.
        glm::mat4 mvpTransform = projectionTransform * modelViewTransform;
        
        // Use the shader program for the skeleton.
        glUseProgram(skeletonShader);
        glUniformMatrix4fv(mvpUniformSkeleton, 1, GL_FALSE, glm::value_ptr(mvpTransform));
        // Draw the skeleton.
        updateAndDrawSkeleton();
        
        
        
        // Use the shader program for cylinder.
        glUseProgram(cylinderShader);
        
        // Update model view.
        glUniformMatrix4fv(mvpUniformCylinder, 1, GL_FALSE, glm::value_ptr(mvpTransform));
        
        // Write the matrices of the skeleton to the uniform.
        std::vector<glm::mat4> skelMatrices;
        skelMatrices.reserve(skeletonCount);
        for (SkelNode & node : skelNodes) {
            skelMatrices.push_back(node.getCalcTransform());
        }
        glUniformMatrix4fv(pointTransformUniform, skeletonCount, GL_FALSE, glm::value_ptr(skelMatrices[0]));
        
        // Bind vertex array and index buffer.
        glBindVertexArray(vao);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
        
        // Draw vertexIndices vertices.
        glDrawElements(sin(timeStep * 0.5f) >= 0 ? GL_LINE_STRIP : GL_TRIANGLE_STRIP, indexCount, GL_UNSIGNED_INT, 0);
        
        // Unbind the buffer and vertex array.
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
        
        glutSwapBuffers();
    }
    
    void MainRoom::reshape(GLsizei width, GLsizei height) {
        glViewport(0.0f, 0.0f, width, height);
        GLfloat aspect = ((GLfloat)width) / ((GLfloat)height);
        // 50 degrees fovy
        projectionTransform = glm::perspective(0.872664626f, aspect, 0.1f, 1000.0f);
    }
    
    void MainRoom::end(void) {
    }
    void MainRoom::keyNormal(unsigned char key, int x, int y) {}
    void MainRoom::keyNormalRelease(unsigned char key, int x, int y) {}
    void MainRoom::keySpecial(int key, int x, int y) {}
    void MainRoom::keySpecialRelease(int key, int x, int y) {}
    void MainRoom::mouseEvent(int button, int state, int x, int y) {}
    void MainRoom::mouseMove(int x, int y) {}
    void MainRoom::mouseDrag(int x, int y) {}
    bool MainRoom::shouldDeleteOnRoomEnd(void) { return false; }

}
