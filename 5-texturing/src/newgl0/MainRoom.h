#ifndef NEWGL_MAINROOM_H_
#define NEWGL_MAINROOM_H_

#include <wool/room/base/RoomBase.h>
#include <threading/ThreadQueue.h>
#include <wool/texture/Texture.h>
#include <glm/mat4x4.hpp>
#include <unordered_map>
#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <GL/gl.h>
#include <vector>
#include <string>
#include <array>
#include <mutex>

#include "TextureManager.h"

namespace newgl {

    class MainRoom : public wool::RoomBase {
    private:
        
        class SimpleAsset {
        private:
            TextureManager * manager;
            glm::mat4 transform;
            std::string textureName;
            void * textureToken;
        public:
            SimpleAsset(TextureManager * manager, glm::mat4 transform, const std::string & textureName);
            ~SimpleAsset(void);
            void requestTex(void);
            void abandonTex(void);
            void draw(GLuint mvpUniform, GLuint textureLayerUniform, GLsizei indexCount, const glm::mat4 & mvpTransform);
        };
        
        // This contains all the data required for each vertex.
        class VertexInfo {
        public:
            glm::vec3 position;
            glm::vec2 uv;
        };
        
        // Make sure the compiler hasn't done anything funny with the layout.
        static_assert(
            sizeof(VertexInfo) == sizeof(GLfloat) * 5 &&
            offsetof(VertexInfo, position) == 0 &&
            offsetof(VertexInfo, uv) == 12
        );
        
        // The vertex array object for feeding data to the shader, from the buffer.
        GLuint vao;
        
        // Buffers for vertices.
        GLuint vertexBuffer, indexBuffer;
        
        // Projection and model-view transforms.
        glm::mat4 projectionTransform, modelViewTransform;
        
        // Our shader programs. These are set up in compileShaders.
        GLuint shaderProgram;
        
        // The number of vertices, and vertex indices.
        GLsizei vertexCount,
                indexCount;
                
        // Uniform locations for matrices and texture layer.
        GLuint mvpUniform, textureLayerUniform;
        
        // For camera movement.
        GLfloat timeStep, timeStep2;
        unsigned int lastRemaining;
        
        // For terrain height adjustment.
        GLfloat totalTimeElapsed;
        
        threading::ThreadQueue threadQueue;
        std::recursive_mutex mutex;
        TextureManager textureManager;
        
        // Assets for testing different textures.
        #define SIMPLE_ASSET_COUNT 8
        SimpleAsset assets[SIMPLE_ASSET_COUNT];
        
    public:
        MainRoom(void);
        virtual ~MainRoom(void);
    private:
        static void compileAndCheckShader(const std::string & name, GLuint shader);
        static void linkAndCheckProgram(const std::string & name, GLuint program);
        static GLuint compileShaderProgram(const std::string & name, const char * vertexShaderSource, const char * geometryShaderSource, const char * fragmentShaderSource);
    
        void initGl3w(void);
        void compileShaders(void);
        void setupUniforms(void);
        void loadVertices(void);
        void setupVertexArray(void);
        
    public:
        virtual void init(void) override;
        virtual void step(void) override;
        virtual void end(void) override;
        virtual void keyNormal(unsigned char key, int x, int y) override;
        virtual void keyNormalRelease(unsigned char key, int x, int y) override;
        virtual void keySpecial(int key, int x, int y) override;
        virtual void keySpecialRelease(int key, int x, int y) override;
        virtual void reshape(GLsizei width, GLsizei height) override;
        virtual void mouseEvent(int button, int state, int x, int y) override;
        virtual void mouseMove(int x, int y) override;
        virtual void mouseDrag(int x, int y) override;
        virtual bool shouldDeleteOnRoomEnd(void) override;

    };
}

#endif
