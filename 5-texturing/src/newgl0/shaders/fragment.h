R"GLSL_END_TOKEN(#version 140

// Inputs (from vertex shader)
in vec2 vfUv;

// Outputs
out vec4 outColour;

// Texture
uniform sampler2DArray tex;

// Layer selection
uniform uint textureLayer;

void main(void) {
    outColour = texture(tex, vec3(vfUv, textureLayer));
}
)GLSL_END_TOKEN"
