/*
 * TextureManager.cpp
 *
 *  Created on: 8 Jan 2020
 *      Author: wilson
 */

#include <GL/gl3w.h> // Include first

#include "TextureManager.h"

#include <wool/texture/lodepng/lodepng.h>
#include <threading/ThreadQueue.h>
#include <functional>
#include <iostream>
#include <cstdlib>
#include <chrono>

namespace newgl {
    
    // -------------------------------- Internal texture -----------------------

    TextureManager::InternalTexture::InternalTexture(std::atomic<unsigned int> * loadRemaining, threading::ThreadQueue * queue, const std::string textureName, unsigned int baseTexSize) :
        loadRemaining(loadRemaining),
        queue(queue),
        token(queue->createToken()),
        baseTexSize(baseTexSize),
        allocation(NULL),
        levels(),
        textureName(textureName),
        loaded(false),
        uses(1),
        layer(0) {
        
        // Increment the number of remaining to load, since we're starting load.
        loadRemaining->fetch_add(1, std::memory_order_relaxed);
        
        // Perform loading in the background.
        queue->add(token, [this](void) {
            performLoad();
        });
    }
    
    TextureManager::InternalTexture::~InternalTexture(void) {
        queue->deleteToken(token);
        free(allocation);
    }
    
    void TextureManager::InternalTexture::performLoad(void) {
        
        // Load texture.
        unsigned char * loadedTextureData = NULL;
        unsigned int loadedTextureSize;
        
        {
            unsigned int loadedTextureHeight;
            unsigned int error = lodepng_decode32_file(&loadedTextureData, &loadedTextureSize, &loadedTextureHeight, textureName.c_str());
            
            // If there was an error print a complaint.
            if (error != 0 || loadedTextureData == NULL) {
                std::cerr << "WARNING: Failed to load texture: " << textureName << ".\n";
            
            // If image is not square, we have an error.
            } else if (loadedTextureSize != loadedTextureHeight) {
                std::cerr << "WARNING: Texture " << textureName << " is not square: (" << loadedTextureSize << 'x' << loadedTextureHeight << ")\n";
                error = 1;
            
            // If image size is not a power of two, it is also an error.
            } else if (!isNumberPowerOfTwo(loadedTextureSize)) {
                std::cerr << "WARNING: Texture " << textureName << " has dimensions which are not powers of two: (" << loadedTextureSize << 'x' << loadedTextureHeight << ")\n";
                error = 1;
            }
            
            // If an error happened, use the "no texture" texture.
            if (error != 0 || loadedTextureData == NULL) {
                free(loadedTextureData);
                // Use the "no texture" texture.
                loadedTextureSize = DEFAULT_NO_TEXTURE_TEXTURE_SIZE;
                loadedTextureData = generateNoTextureTexture(loadedTextureSize);
            }
        }
        
        // After this point, we assume that we have a valid, power of two, square texture,
        // of size loadedTextureSize.
        // Allocate an internal block of data for the texture.
        unsigned char * internalAllocation = (unsigned char *)malloc(getMipMapTexAllocSize(baseTexSize));
        if (internalAllocation == NULL) {
            std::cerr << "ERROR: Failed to allocate memory for texture " << textureName << ".\n";
            exit(EXIT_FAILURE);
        }
        // Populate a vector of pointers to appropriate places within this allocation.
        std::vector<unsigned char *> internalLevels;
        internalLevels.reserve(getMipMapLevels(baseTexSize));
        for (size_t offset : getMipMapOffsets(baseTexSize)) {
            internalLevels.push_back(internalAllocation + offset);
        }
        // Populate the allocation with scaled versions of our original image.
        unsigned int level = 0;
        for (unsigned int textureSize : getMipMapTextureSizes(baseTexSize)) {
            // If first level, or making image bigger, scale image from original loaded texture.
            if (textureSize >= loadedTextureSize || level == 0) {
                scaleImage(internalLevels[level], textureSize, loadedTextureData, loadedTextureSize);
            // Otherwise, scale image from previous level, as that is faster to do.
            } else {
                scaleImage(internalLevels[level], textureSize, internalLevels[level - 1], textureSize * 2);
            }
            level++;
        }
        // We don't need the original allocation any more.
        free(loadedTextureData);
        // Store results.
        allocation = internalAllocation;
        levels = std::move(internalLevels);
        // Decrement the number of remaining to load, since we're finishing load.
        loadRemaining->fetch_sub(1, std::memory_order_relaxed);
        // Finally, set loaded to true. Use std::memory_order_release to ensure all results are written before this.
        loaded.store(true, std::memory_order_release);
    }
    
    void TextureManager::InternalTexture::loadMipMapLevelIntoGpu(unsigned int textureLayer, unsigned int mipMapLevel) {
        unsigned int size = getMipMapTextureSize(baseTexSize, mipMapLevel);
        glTexSubImage3D(
            GL_TEXTURE_2D_ARRAY, // Texture target.
            mipMapLevel,         // Detail level for mipmapping.
            0, 0,                // Offset (x,y).
            textureLayer,        // Offset (z): Texture layer to load into.
            size, size,          // Size (width, height).
            1,                   // Depth of data to add (1 since we only do one mipmap level at once.
            GL_RGBA,             // Pixel format
            GL_UNSIGNED_BYTE,    // Pixel type
            levels[mipMapLevel]  // Pixel data
        );
    }
    
    bool TextureManager::InternalTexture::isLoaded(void) const {
        return loaded.load(std::memory_order_acquire);
    }
    
    // -------------------------------------------------------------------------

    TextureManager::TextureManager(threading::ThreadQueue * queue, unsigned int baseTexSize) :
        queue(queue),
        token(queue->createToken()),
        baseTexSize(baseTexSize),
        internalTextures(),
        disusedTextures(),
        textureId(0),
        prospectiveTextureId(0),
        loading(false),
        prospectiveOrder(),
        textureStoreUpTo(0),
        textureStoreMipmapUpTo(0),
        loadRemaining(0) {
        
        if (!isNumberPowerOfTwo(baseTexSize)) {
            std::cerr << "ERROR: TextureManager constructor: " << baseTexSize << " is NOT a power of two.\n";
            exit(EXIT_FAILURE);
        }
    }
    
    TextureManager::~TextureManager(void) {
        // Delete prospective and normal texture IDs if they exist.
        if (prospectiveTextureId != 0) glDeleteTextures(1, &prospectiveTextureId);
        if (textureId != 0) glDeleteTextures(1, &textureId);
        // Delete internal textures.
        for (const std::pair<std::string, InternalTexture *> & pair : internalTextures) {
            delete pair.second;
        }
        // Delete our token.
        queue->deleteToken(token);
    }
    
    void TextureManager::removeDisusedTextures(void) {
        // Do nothing and return if there are no disused textures.
        if (disusedTextures.empty()) return;
        // Delete and erase from data structures, all disused textures.
        for (const std::string & name : disusedTextures) {
            auto iter = internalTextures.find(name);
            delete iter->second;
            internalTextures.erase(iter);
        }
        // Clear disused textures set, and reset loading, since textures have changed.
        disusedTextures.clear();
        resetLoading = true;
    }

    void TextureManager::doResetLoading(void) {
        resetLoading = false;
        // Delete prospective texture if it exists.
        if (prospectiveTextureId != 0) {
            glDeleteTextures(1, &prospectiveTextureId);
            prospectiveTextureId = 0;
        }
        // Clear load progress.
        prospectiveOrder.clear();
        textureStoreUpTo = 0;
        textureStoreMipmapUpTo = 0;
        // Set to loading if there are internal textures.
        loading = !internalTextures.empty();
    }

    void TextureManager::initLoading(void) {
        // Generate prospective order.
        for (const std::pair<std::string, InternalTexture *> & pair : internalTextures) {
            prospectiveOrder.push_back(pair.second);
        }
        
        // Generate a texture name.
        glGenTextures(1, &prospectiveTextureId);
        // Initialise the texture as a 2d texture array, and bind.
        glBindTexture(GL_TEXTURE_2D_ARRAY, prospectiveTextureId);
        
        // Set texture parameters.
        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        
        // Allocate memory for the texture array. Repeat this for each mipmap level.
        unsigned int level = 0;
        for (unsigned int mipMapSize : getMipMapTextureSizes(baseTexSize)) {
            glTexImage3D(
                GL_TEXTURE_2D_ARRAY,      // Target
                level,                    // Mipmap level to specify.
                GL_RGBA8,                 // Internal format
                mipMapSize, mipMapSize,   // Size of this mipmap level (width, height)
                prospectiveOrder.size(),  // Depth (number of textures in array): One for each texture we have.
                0,                        // Border (nobody knows what it does, but must be 0).
                GL_RGBA,                  // General format of pixel data (layout)
                GL_UNSIGNED_BYTE,         // Data type of pixel data
                NULL                      // Initial data (specify as null since we provide the data later with glTexSubImage3D.
            );
            level++;
        }
        
        // It no longer needs to be bound.
        glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
    }
    
    void TextureManager::doLoading(unsigned int maxTime) {
        // Bind the texture so we know what to load data into.
        glBindTexture(GL_TEXTURE_2D_ARRAY, prospectiveTextureId);
        // Iterate until we run out of textures or exceed max time.
        unsigned int levels = getMipMapLevels(baseTexSize);
        std::chrono::time_point<std::chrono::steady_clock> startTime = std::chrono::steady_clock::now();
        while(textureStoreUpTo < prospectiveOrder.size() && std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - startTime).count() <= maxTime) {
            
            // Get texture at this position, end loop if it isn't loaded yet.
            InternalTexture * tex = prospectiveOrder[textureStoreUpTo];
            if (!tex->isLoaded()) break;
            
            // Tell the texture to load the mipmap level into the gpu.
            tex->loadMipMapLevelIntoGpu(textureStoreUpTo, textureStoreMipmapUpTo);
            
            // Increment upto variables, (iterate textureStoreMipmapUpTo, when that reaches end loop around and increment textureStoreUpTo).
            if (textureStoreMipmapUpTo == levels - 1) {
                textureStoreMipmapUpTo = 0;
                textureStoreUpTo++;
            } else {
                textureStoreMipmapUpTo++;
            }
        }
        glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
    }
    
    void TextureManager::runLoadCompletion(void) {
        // Delete old texture if it exists.
        if (textureId != 0) {
            glDeleteTextures(1, &textureId);
        }
        // Use prospectiveTextureId as new texture.
        textureId = prospectiveTextureId;
        prospectiveTextureId = 0;
        // Set to false since loading completed.
        loading = false;
        // Set the texture layer id of each internal texture.
        unsigned int textureLayerId = 0;
        for (InternalTexture * tex : prospectiveOrder) {
            tex->setTextureLayer(textureLayerId);
            textureLayerId++;
        }
    }

    void * TextureManager::requestTexture(const std::string & filename) {
        auto iter = internalTextures.find(filename);
        if (iter == internalTextures.end()) {
            // If the texture does not exist, create it, add it to internal textures, and return it.
            InternalTexture * tex = new InternalTexture(&loadRemaining, queue, filename, baseTexSize);
            internalTextures[filename] = tex;
            resetLoading = true; // Reset loading since a new texture has been requested.
            return tex;
        } else {
            // If the texture already exists, add a use and return it. Make sure the texture is
            // no longer disused.
            disusedTextures.erase(filename);
            iter->second->addUse();
            return iter->second;
        }
    }
    
    void TextureManager::abandonTexture(void * textureToken) {
        InternalTexture * texture = (InternalTexture *)textureToken;
        texture->removeUse();
        if (!texture->isUsed()) {
            // If the texture is disused, add it to the set of disused textures.
            disusedTextures.insert(texture->getTextureName());
        }
    }
    
    unsigned int TextureManager::step(unsigned int maxTime) {
        removeDisusedTextures();
        if (resetLoading) {
            doResetLoading();
        }
        if (loading) {
            if (prospectiveTextureId == 0) {
                initLoading();
            }
            doLoading(maxTime);
            if (textureStoreUpTo == prospectiveOrder.size()) {
                runLoadCompletion();
            }
        }
        glBindTexture(GL_TEXTURE_2D_ARRAY, textureId);
        return loadRemaining.load(std::memory_order_relaxed) + (loading ? 1 : 0);
    }
}
