R"GLSL_END_TOKEN(#version 440 core

// Input and output.
in vec4 vfColour;
out vec4 outColour;

// Texture input
uniform sampler2D tex;

void main(void) {
    if (vfColour.a == 0.0f) {
        // Use the colour from the texture.
        outColour = texelFetch(tex, ivec2(gl_FragCoord.xy) % textureSize(tex, 0), 0);
    } else {
        // Multiply the red, green and blue channels by 10, mod them.
        vec3 col = vec3(vfColour.r, vfColour.g, vfColour.b);
        col = mod(col * 10, 1.0f);
        outColour = vec4(col, 1.0f);
    }
}
)GLSL_END_TOKEN"
