import java.nio.charset.StandardCharsets;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.awt.Color;
import java.io.File;

class ColourDef {
    public int value, id;
    public float blurryness;
    public static ColourDef[] defs;
    public static boolean foundWrongColours = false;
    public ColourDef(int value, int id, float blurryness) {
        this.value = value;
        this.id = id;
        this.blurryness = blurryness;
    }
    public static void genColourDefs() throws Exception {
        List<String> lines = Files.readAllLines(Paths.get("colourDefs"), StandardCharsets.UTF_8);
        List<ColourDef> cDefs = new ArrayList<ColourDef>();
        int i = 0;
        for (String line : lines) {
            if (i != 0) {
                String[] strArr = line.split(" ");
                cDefs.add(new ColourDef(Integer.parseInt(strArr[1]), i - 1, Float.parseFloat(strArr[2])));
            }
            i++;
        }
        defs = new ColourDef[cDefs.size()];
        cDefs.toArray(defs);
    }
    public static ColourDef find(int x, int y, Color colour) {
        int val = colour.getRed();
        if (colour.getGreen() != val || colour.getBlue() != val) {
            System.err.println("ERROR: Colour map: Pixel ("+x+","+y+") not grey.");
            System.exit(1);
        }
        if (colour.getAlpha() != 255) {
            System.err.println("ERROR: Colour map: Pixel ("+x+","+y+") contains transparency.");
            foundWrongColours = true;
            return null;
        }
        for (ColourDef def : defs) {
            if (def.value == val) {
                return def;
            }
        }
        System.err.println("ERROR: Colour map: Pixel ("+x+","+y+") is unrecognised colour.");
        foundWrongColours = true;
        return null;
    }
}

class WeightedColPair {
    public ColourDef colDef1, colDef2;
    public int weight1, weight2;
    public WeightedColPair(ColourDef colDef1, int weight1, ColourDef colDef2, int weight2) {
        if (colDef1.id < colDef2.id) {            
            this.colDef1 = colDef1;
            this.colDef2 = colDef2;
            this.weight1 = weight1;
            this.weight2 = weight2;
        } else {
            this.colDef1 = colDef2;
            this.colDef2 = colDef1;
            this.weight1 = weight2;
            this.weight2 = weight1;
        }
    }
    public WeightedColPair() {}
    public int getBlurinessRadius() {
        return (int)(Math.ceil((colDef1.blurryness * colDef2.blurryness) * ((float)Pix.BASE_BLUR_RAD)));
    }
    public Color getFinalColour(int height) {
        // 0 is col1, 255 is col2.
        int mix = 255 - Math.round((((float)weight1) / ((float)(weight1 + weight2))) * 255.0f);
        // height, mix, terrain1, terrain2.
        return new Color(height, mix, colDef1.value, colDef2.value);
    }
    // Returns true if one of our weights is zero.
    public boolean isOneSided() {
        return weight1 == 0 || weight2 == 0;
    }
    // Returns the colour assuming one of our weights is zero.
    public ColourDef getSingleColour() { if (weight1 > 0) return colDef1; else return colDef2; }
}

class Pix {
    public ColourDef colour;
    public int height, x, y;
    public final static int BASE_BLUR_RAD = 8;
    private WeightedColPair pair, normalisedPair;
    public Pix(int x, int y, Color heightCol, Color colourCol) {
        height = readHeight(x, y, heightCol);
        colour = ColourDef.find(x, y, colourCol);
        this.x = x; this.y = y;
        pair = null;
        normalisedPair = null;
    }
    public static int readHeight(int x, int y, Color colour) {
        int val = colour.getRed();
        if (colour.getGreen() != val || colour.getBlue() != val) {
            System.err.println("ERROR: Height map: Pixel ("+x+","+y+") not grey.");
            ColourDef.foundWrongColours = true;
        }
        if (colour.getAlpha() != 255) {
            System.err.println("ERROR: Height map: Pixel ("+x+","+y+") contains transparency.");
            ColourDef.foundWrongColours = true;
        }
        return val;
    }
    // Generates our internal colour pair.
    public void generateColourPair(Pix[][] pixels, int pixImageWidth, int pixImageHeight) {
        // Work out the pair of colours, based on blurriness around surrounding colours.
        int radius = BASE_BLUR_RAD;
        while(radius >= 1) {
            pair = getMostCommonPair(pixels, pixImageWidth, pixImageHeight, radius);
            if (pair != null && pair.getBlurinessRadius() >= radius) {
                break;
            }
            radius--;
        }
    }
    
    // Looks at one-sided single colour pixels.
    // Finds surrounding pixels with at least one colour difference. Swaps if necessary,
    // and sets our zero colour to match theirs.
    public void firstLevelNormalise(Pix[][] pixels, int pixImageWidth, int pixImageHeight) {
        if (!pair.isOneSided() || pair.colDef1 != pair.colDef2) {
            normalisedPair = pair;
            return;
        }
        
        ColourDef ourSingleColour = pair.getSingleColour();
        int maxWeight = 0;
        
        // Look at ALL adjacent.
        for (int dy = -1; dy <= 1; dy++) {
            for (int dx = -1; dx <= 1; dx++) {
                int newX = x + dx, newY = y + dy;
                if (newX >= 0 && newY >= 0 && newX < pixImageWidth && newY < pixImageHeight && dx != 0 && dy != 0) {
                    Pix other = pixels[newX][newY];
                    // Found and adjacent pixel with at least one colour different.
                    if (other.pair.colDef1 != pair.colDef1 || other.pair.colDef2 != pair.colDef2) {
                        
                        normalisedPair = new WeightedColPair();
                        normalisedPair.colDef1 = pair.colDef1;
                        normalisedPair.colDef2 = pair.colDef2;
                        
                        // Swap if needed.
                        if ((pair.weight1 > 0 && pair.colDef1 == other.pair.colDef1) ||
                            (pair.weight2 > 0 && pair.colDef2 == other.pair.colDef2)) {
                            // Colours same way round - no change needed - no swap.
                            normalisedPair.weight1 = pair.weight1;
                            normalisedPair.weight2 = pair.weight2;
                        } else {
                            // Colours opposite way round - swap.
                            normalisedPair.weight1 = pair.weight2;
                            normalisedPair.weight2 = pair.weight1;
                        }
                        
                        // Set zero colour.
                        if (normalisedPair.weight1 == 0) {
                            normalisedPair.colDef1 = other.pair.colDef1;
                        } else if (normalisedPair.weight2 == 0) {
                            normalisedPair.colDef2 = other.pair.colDef2;
                        }
                        
                        return;
                    }
                }
            }
        }
        
        // If found nothing don't change pair.
        normalisedPair = pair;
    }
    
    // Looks at one-sided single colour pixels.
    // Finds surrounding pixels with at least one colour difference. Swaps if necessary,
    // BUT DOES NOT SET OUR ZERO COLOUR.
    public void secondLevelNormalise(Pix[][] pixels, int pixImageWidth, int pixImageHeight) {
        if (!pair.isOneSided() || pair.colDef1 != pair.colDef2) {
            normalisedPair = pair;
            return;
        }
        
        ColourDef ourSingleColour = pair.getSingleColour();
        int maxWeight = 0;
        
        // Look at ALL adjacent.
        for (int dy = -1; dy <= 1; dy++) {
            for (int dx = -1; dx <= 1; dx++) {
                int newX = x + dx, newY = y + dy;
                if (newX >= 0 && newY >= 0 && newX < pixImageWidth && newY < pixImageHeight && dx != 0 && dy != 0) {
                    Pix other = pixels[newX][newY];
                    // Found and adjacent pixel with at least one colour different.
                    if (other.pair.colDef1 != pair.colDef1 || other.pair.colDef2 != pair.colDef2) {
                        
                        normalisedPair = new WeightedColPair();
                        normalisedPair.colDef1 = pair.colDef1;
                        normalisedPair.colDef2 = pair.colDef2;
                        
                        // Swap if needed.
                        if ((pair.weight1 > 0 && pair.colDef1 == other.pair.colDef1) ||
                            (pair.weight2 > 0 && pair.colDef2 == other.pair.colDef2)) {
                            // Colours same way round - no change needed - no swap.
                            normalisedPair.weight1 = pair.weight1;
                            normalisedPair.weight2 = pair.weight2;
                        } else {
                            // Colours opposite way round - swap.
                            normalisedPair.weight1 = pair.weight2;
                            normalisedPair.weight2 = pair.weight1;
                        }
                        // Don't set a zero colour, or a seam happens.
                        return;
                    }
                }
            }
        }
        
        // If found nothing don't change pair.
        normalisedPair = pair;
    }
        
    // Sets our pair to our normalised pair.
    public void applyNormalised() {
        pair = normalisedPair;
        normalisedPair = null;
    }
    // Gets our final output.
    public Color getOutput() {
        return pair.getFinalColour(height);
    }
    // Finds the most common pair of colours nearby. radius must be 1 or bigger.
    private WeightedColPair getMostCommonPair(Pix[][] pixels, int pixImageWidth, int pixImageHeight, int radius) {
        int[] counts = new int[ColourDef.defs.length];
        for (int dy = -radius; dy <= radius; dy++) {
            for (int dx = -radius; dx <= radius; dx++) {
                int newX = x + dx, newY = y + dy;
                if (newX >= 0 && newY >= 0 && newX < pixImageWidth && newY < pixImageHeight &&
                    dx * dx + dy * dy <= radius * radius) {
                    counts[pixels[newX][newY].colour.id]++;
                }
            }
        }
        // Find largest
        int firstLargestId = -1,
            firstLargestCount = Integer.MIN_VALUE,
            secondLargestId = -1,
            secondLargestCount = Integer.MIN_VALUE,
            nonZeroCount = 0;
        for (int i = 0; i < counts.length; i++) {
            if (counts[i] > 0) {
                nonZeroCount++;
            }
            if (counts[i] > firstLargestCount) {
                firstLargestId = i;
                firstLargestCount = counts[i];
            }
        }
        // Try to avoid situations where there are more than 2 colours to mix,
        // until the radius is small enough that it cannot be avoided.
        if (nonZeroCount > 2 && radius > 1) return null;
        // Find second largest
        counts[firstLargestId] = Integer.MIN_VALUE;
        for (int i = 0; i < counts.length; i++) {
            if (counts[i] > secondLargestCount) {
                secondLargestId = i;
                secondLargestCount = counts[i];
            }
        }
        if (secondLargestCount == 0) secondLargestId = firstLargestId;
        return new WeightedColPair(ColourDef.defs[firstLargestId], firstLargestCount, ColourDef.defs[secondLargestId], secondLargestCount);
    }
}


public class ColourConv {
    static long lastProgressTime = 0;
    private static void printProgress(float percent) {
        long currentTime = System.currentTimeMillis();
        if (currentTime - lastProgressTime > 100) {
            lastProgressTime = currentTime;
            System.out.printf("    %.2f%%...\n", percent);
        }
    }
    
    public static void main(String[] args) throws Exception {
        ColourDef.genColourDefs();
        
        // Load images, check sizes.
        System.out.println("Reading images...");
        BufferedImage colourImage = ImageIO.read(new File("colours.png")),
                      heightImage = ImageIO.read(new File("heightmap.png"));
        int width = colourImage.getWidth(),
            height = colourImage.getHeight();
        if (heightImage.getWidth() != width || heightImage.getHeight() != height) {
            System.err.println("ERROR: Sizes of colour and height map images do not match.");
            System.exit(1);
        }
        
        // Build array of pixels.
        System.out.println("Building pixel data...");
        Pix[][] pixels = new Pix[width][];
        for (int i = 0; i < height; i++) { pixels[i] = new Pix[height]; }
        
        // Fill array of pixels.
        for (int iy = 0; iy < height; iy++) {
            for (int ix = 0; ix < width; ix++) {
                pixels[ix][iy] = new Pix(ix, iy, new Color(heightImage.getRGB(ix, iy), true), new Color(colourImage.getRGB(ix, iy), true));
                printProgress(((((float)iy) * width + ((float)ix)) * 100) / (width * height));
            }
        }
        
        // Generating colour pairs.
        System.out.println("Generating colour pairs...");
        for (int iy = 0; iy < height; iy++) {
            for (int ix = 0; ix < width; ix++) {
                pixels[ix][iy].generateColourPair(pixels, width, height);
                printProgress(((((float)iy) * width + ((float)ix)) * 100) / (width * height));
            }
        }
        
        // Normalising colour pairs.
        System.out.println("Normalising colour pairs (first)...");
        for (int iy = 0; iy < height; iy++) {
            for (int ix = 0; ix < width; ix++) {
                pixels[ix][iy].firstLevelNormalise(pixels, width, height);
                printProgress(((((float)iy) * width + ((float)ix)) * 100) / (width * height));
            }
        }
        for (int iy = 0; iy < height; iy++) {
            for (int ix = 0; ix < width; ix++) {
                pixels[ix][iy].applyNormalised();
            }
        }
        
        // Normalising colour pairs.
        System.out.println("Normalising colour pairs (second)...");
        for (int iy = 0; iy < height; iy++) {
            for (int ix = 0; ix < width; ix++) {
                pixels[ix][iy].secondLevelNormalise(pixels, width, height);
                printProgress(((((float)iy) * width + ((float)ix)) * 100) / (width * height));
            }
        }
        for (int iy = 0; iy < height; iy++) {
            for (int ix = 0; ix < width; ix++) {
                pixels[ix][iy].applyNormalised();
            }
        }
        
        // Getting output from pixels.
        System.out.println("Extracting output...");
        BufferedImage outputImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        for (int iy = 0; iy < height; iy++) {
            for (int ix = 0; ix < width; ix++) {
                outputImage.setRGB(ix, iy, pixels[ix][iy].getOutput().getRGB());
                printProgress(((((float)iy) * width + ((float)ix)) * 100) / (width * height));
            }
        }
        
        // Save image.
        System.out.println("Saving output image...");
        ImageIO.write(outputImage, "png", new File("map.png"));
    }
}
