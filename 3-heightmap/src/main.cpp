
#include <GL/gl3w.h>
#include <GL/freeglut.h>
#include <iostream>
#include <cstdlib>

#include <wool/window/config/WindowConfig.h>
#include <wool/window/base/WindowBase.h>

#include "newgl0/MainRoom.h"

int main(int argc, char * argv[]) {

    {
        newgl::MainRoom room;
        wool::WindowConfig config;
        config.windowTitle = (char *)"OpenGL!";
        config.enableWindozeVsync = true;
        config.width = 640;
        config.height = 480;
        config.displayMode = GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH;
        wool::WindowBase windowBase(&room, config);
        windowBase.enableFrameRateLogging = true;
        windowBase.start(&argc, argv);
        room.end();
    }
    std::cout << "Program done.\n";
    return EXIT_SUCCESS;
}
