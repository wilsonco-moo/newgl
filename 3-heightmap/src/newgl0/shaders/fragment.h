R"GLSL_END_TOKEN(#version 140

// He many cells each grid square covers.
#define TEXTURE_EXTEND 4

// Inputs (from vertex shader)
in float vfMix;
flat in int vfTextureId1;
flat in int vfTextureId2;
in float vfX;
in float vfY;

// Outputs
out vec4 outColour;

// Texture
uniform sampler2DArray tex;

void main(void) {
    
    float x = mod(vfX / TEXTURE_EXTEND, 1.0f),
          y = mod(vfY / TEXTURE_EXTEND, 1.0f);
    
    vec4 col1 = texture(tex, vec3(x, y, vfTextureId1)),
         col2 = texture(tex, vec3(x, y, vfTextureId2));
    
    outColour = mix(col1, col2, vfMix);
}
)GLSL_END_TOKEN"
