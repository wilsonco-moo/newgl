#version 140

// Inputs (vertex attributes).
in float inHeight;
in float inMix;
in int inTextureId1;
in int inTextureId2;

// Outputs (to fragment shader).
out float vfMix;
flat out int vfTextureId1;
flat out int vfTextureId2;
out float vfX;
out float vfY;

// Uniforms
uniform mat4 mvpTransformMatrix;
uniform int worldSize;
uniform float worldHeight;

void main(void) {
    
    // Get coordinate within world from vertex id.
    // Note that the vertex ID is the index within the vertex array, and the
    // vertex shader is not run multiple times for each index, even if that
    // index is repeated in the elements buffer.
    int x = gl_VertexID % worldSize,
        y = gl_VertexID / worldSize;

    gl_Position = mvpTransformMatrix * vec4(x, y, inHeight * worldHeight, 1.0f);
    
    vfMix = inMix;
    vfTextureId1 = inTextureId1;
    vfTextureId2 = inTextureId2;
    vfX = x;
    vfY = y;
}
