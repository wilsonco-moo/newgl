#include <GL/gl3w.h> // Include first

#include "MainRoom.h"

#include <wool/texture/lodepng/lodepng.h>
#include <wool/window/base/WindowBase.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/freeglut.h>
#include <glm/vec3.hpp>
#include <iostream>
#include <cstdlib>
#include <GL/gl.h>

namespace newgl {
    
    #define X(id, name, colourValue) {colourValue, id},
    const std::unordered_map<uint32_t, GLubyte> MainRoom::texColToId = {
        TEXTURE_DEF
    };
    #undef X
    
    MainRoom::MainRoom(void) :
        vao(0),
        vertexDataBuffer(0),
        vertexIndexBuffer(0),
        projectionTransform(),
        modelViewTransform(),
        shaderProgram(0),
        worldSize(0),
        vertexIndices(0),
        textureArr(0),
        mvpTransformMatrixUniform(0),
        worldSizeUniform(0),
        timeStep(0.0f),
        totalTimeElapsed(0.0f) {
    }

    MainRoom::~MainRoom(void) {
        
    }
    
    GLubyte MainRoom::colourToTexId(unsigned int x, unsigned int y, uint32_t colourValue) {
        auto iter = texColToId.find(colourValue);
        if (iter == texColToId.end()) {
            std::cerr << "Unknown colour value " << colourValue << " found at texture coordinate (" << x << ',' << y << ").\n";
            exit(EXIT_FAILURE);
            return 0;
        } else {
            return iter->second;
        }
    }
    
    void MainRoom::compileAndCheckShader(const char * name, GLuint shader) {
        // Try to compile the shader.
        glCompileShader(shader);
        
        // If there is a log, print it on stderr.
        GLint len;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &len);
        if (len != 0) {
            std::string str(len, ' ');
            glGetShaderInfoLog(shader, len, NULL, &str[0]);
            std::cerr << str;
        }
        
        // If it didn't compile, complain.
        GLint isCompiled = 0;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
        if (isCompiled == GL_FALSE) {
            std::cerr << "ERROR: Failed to compile " << name << ".\n";
            exit(EXIT_FAILURE);
        }
    }
    
    void MainRoom::linkAndCheckProgram(const char * name, GLuint program) {
        // Try to link the shader.
        glLinkProgram(program);
        
        // If there is a log, print it on stderr.
        GLint len;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &len);
        if (len != 0) {
            std::string str(len, ' ');
            glGetProgramInfoLog(program, len, NULL, &str[0]);
            std::cerr << str;
        }
        
        // If it didn't link, complain.
        GLint isLinked = 0;
        glGetProgramiv(program, GL_LINK_STATUS, &isLinked);
        if (isLinked == GL_FALSE) {
            std::cerr << "ERROR: Failed to link " << name << ".\n";
            exit(EXIT_FAILURE);
        }
    }

    void MainRoom::init(void) {
        initGl3w();
        compileShaders();
        setupUniforms();
        loadWorldTextures();
        loadWorldBuffer();
        setupVertexArray();
        // Misc initialisation
        glEnable(GL_DEPTH_TEST);
    }
    
    void MainRoom::initGl3w(void) {
        if (gl3wInit()) {
            std::cerr << "Failed to initialize gl3w.\n";
            exit(EXIT_FAILURE);
        }
        if (!gl3wIsSupported(3, 1)) {
            std::cerr << "OpenGL 3.1 not supported\n";
            exit(EXIT_FAILURE);
        }
    }
    
    void MainRoom::compileShaders(void) {
        std::cout << "Compiling shaders...\n";
        
        // String literals for shader sources.
        const char * vertexShaderSource = 
        #include "shaders/vertex.h"
        ;
        const char * fragmentShaderSource = 
        #include "shaders/fragment.h"
        ;
        
        // Create and compile vertex shader.
        GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
        compileAndCheckShader("vertex shader", vertexShader);
        
        // Create and compile fragment shader.
        GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
        compileAndCheckShader("fragment shader", fragmentShader);
        
        // Create shader program, attach both shaders and link it.
        shaderProgram = glCreateProgram();
        glAttachShader(shaderProgram, vertexShader);
        glAttachShader(shaderProgram, fragmentShader);
        linkAndCheckProgram("main shader program", shaderProgram);
        
        // Delete the compiled shaders: We don't need them any more since they
        // are part of the program.
        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);
        std::cout << "Done.\n";
    }
    
    void MainRoom::setupUniforms(void) {
        mvpTransformMatrixUniform = glGetUniformLocation(shaderProgram, "mvpTransformMatrix");
        worldSizeUniform = glGetUniformLocation(shaderProgram, "worldSize");
        worldHeightUniform = glGetUniformLocation(shaderProgram, "worldHeight");
    }

    void MainRoom::loadWorldTextures(void) {
        
        unsigned char * textureData[TEXTURES_COUNT];
        unsigned int width, height;
        
        // Load each texture into textureData, using the X macro.
        #define X(id, name, colourValue) {                                                                 \
            std::cout << ("Loading texture: " TEXTURES_LOC #name ".png ...\n");                            \
            unsigned int thisW, thisH,                                                                     \
            error = lodepng_decode32_file(textureData + (id), &thisW, &thisH, TEXTURES_LOC #name ".png");  \
            if (error != 0) {                                                                              \
                std::cerr << ("ERROR: Failed to load texture " TEXTURES_LOC #name ".png.\n");              \
                exit(EXIT_FAILURE);                                                                        \
            }                                                                                              \
            if ((id) == 0) {                                                                               \
                width = thisW;                                                                             \
                height = thisH;                                                                            \
            } else {                                                                                       \
                if (thisW != width || thisH != height) {                                                   \
                    std::cerr << ("ERROR: Texture" TEXTURES_LOC #name ".png has an inconsistent size.\n"); \
                }                                                                                          \
            }                                                                                              \
        }
        TEXTURE_DEF
        #undef X
        
        // Generate a texture name.
        glGenTextures(1, &textureArr);
        // Initialise the texture as a 2d texture array, and bind.
        glBindTexture(GL_TEXTURE_2D_ARRAY, textureArr);
        
        // Set texture parameters.
        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        
        // Allocate memory for the textures.
        glTexImage3D(
            GL_TEXTURE_2D_ARRAY, // Target
            0,                   // Level to specify data for, (this doesn't matter since we don't immediately specify data).
            GL_RGBA8,            // Internal format
            width, height,       // size
            TEXTURES_COUNT,      // depth (number of textures in array).
            0,                   // border (nobody knows what it does, but must be 0).
            GL_RGBA,             // General format of pixel data (layout)
            GL_UNSIGNED_BYTE,    // Data type of pixel data
            NULL                 // Initial data (specify as null since we provide the data later with glTexSubImage3D.
        );

        // Write all the pixel data to the GPU for each texture.
        std::cout << "Building textures...\n";
        for (unsigned int i = 0; i < TEXTURES_COUNT; i++) {
            glTexSubImage3D(
                GL_TEXTURE_2D_ARRAY,
                0,                // Detail level for mipmapping (put data in top level).
                0, 0,             // Offset (x,y)
                i,                // Offset (z)
                width, height,    // Size
                1,                // Depth of data to add (1 since we only do one texture at once
                GL_RGBA,          // Pixel format
                GL_UNSIGNED_BYTE, // Pixel type
                textureData[i]    // Pixel data
            );
            // Free the copy we have in RAM memory each time.
            free(textureData[i]);
        }
        std::cout << "Generating mipmaps...\n";
        glGenerateMipmap(GL_TEXTURE_2D_ARRAY);
        std::cout << "Done.\n";
    }

    void MainRoom::loadWorldBuffer(void) {
        // Load the map image, complain if it is not square.
        wool::Texture mapTexture("assets/map.png", true, false);
        std::cout << "Generating vertex buffer...\n";
        if (!mapTexture.isLoaded()) {
            std::cerr << "ERROR: Failed to load texture assets/map.png.\n";
            exit(1);
        }
        worldSize = (GLuint)mapTexture.getWidth();
        if ((GLuint)mapTexture.getHeight() != worldSize) {
            std::cerr << "ERROR: Map texture is not square.\n";
            exit(1);
        }
        
        // Set the restart index to one after the end, and enable primitive restart.
        /*GLuint restartIndex = worldSize * worldSize;
        glPrimitiveRestartIndex(restartIndex);
        glEnable(GL_PRIMITIVE_RESTART);*/
        
        // Use first vertex convention.
        glProvokingVertex(GL_FIRST_VERTEX_CONVENTION);
        
        // Work out size of vertex and vertex index data.
        GLsizei vertexDataSize = worldSize * worldSize * sizeof(VertexInfo);
        vertexIndices = (worldSize - 1) * (worldSize - 1) * 6;
        GLsizei vertexIndicesSize = vertexIndices * sizeof(GLuint);
        
        // Create and bind buffers for vertex data and vertex index data.
        glGenBuffers(1, &vertexDataBuffer);
        glGenBuffers(1, &vertexIndexBuffer);
        
        glBindBuffer(GL_ARRAY_BUFFER, vertexDataBuffer);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertexIndexBuffer);
        
        glBufferData(GL_ARRAY_BUFFER, vertexDataSize, NULL, GL_STATIC_READ);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, vertexIndicesSize, NULL, GL_STATIC_READ);
        
        void * mappedVertexData = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY),
             * mappedIndexData = glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);
        
        // Cast appropriately so we can write to the buffer.
        VertexInfo * vertexData = (VertexInfo *)mappedVertexData;
        GLuint * indexData = (GLuint *)mappedIndexData;

        // Go through each point.
        for (GLuint iy = 0; iy < worldSize; iy++) {
            for (GLuint ix = 0; ix < worldSize; ix++) {
                
                // Store the vertex data for this index.
                uint32_t colour = mapTexture.getPixelInt(ix, iy);
                vertexData->height     = (GLubyte)((colour >> 24) & 255);            // red   (height)
                vertexData->mix        = (GLubyte)((colour >> 16) & 255);            // green (mix)
                vertexData->textureId1 = colourToTexId(ix, iy, (colour >> 8) & 255); // blue  (texture id 1)
                vertexData->textureId2 = colourToTexId(ix, iy, colour & 255);        // alpha (texture id 2)
                vertexData++;
                
                // Don't store vertex indices for the last row or the last column.
                if (iy != worldSize - 1 && ix != worldSize - 1) {
                    // Draw two triangles for each square.
                    *(indexData++) = (iy * worldSize + ix);
                    *(indexData++) = ((iy + 1) * worldSize + ix);
                    *(indexData++) = ((iy + 1) * worldSize + ix + 1);
                    *(indexData++) = (iy * worldSize + ix);
                    *(indexData++) = (iy * worldSize + ix + 1);
                    *(indexData++) = ((iy + 1) * worldSize + ix + 1);
                }
            }
        }
        
        // Unmap the buffers now we have finished writing to them
        glUnmapBuffer(GL_ARRAY_BUFFER);
        glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
        
        // These don't need to be bound any more.
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        std::cout << "Done.\n";
    }
    
    void MainRoom::setupVertexArray(void) {
        
        // Get the locations within the vertex shader of the 4 inputs.
        GLuint heightLoc = glGetAttribLocation(shaderProgram, "inHeight"),
               mixLoc    = glGetAttribLocation(shaderProgram, "inMix"),
               texId1Loc = glGetAttribLocation(shaderProgram, "inTextureId1"),
               texId2Loc = glGetAttribLocation(shaderProgram, "inTextureId2");
        
        // Create and bind a vertex array object.
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);
        
        // Bind the array buffer, so the glVertexAttribPointer calls know where to go.
        glBindBuffer(GL_ARRAY_BUFFER, vertexDataBuffer);
        
        // Create each of the 4 attributes. Height and should come through as normalised floats, the other two
        // should come through just as integers.
        glVertexAttribPointer (heightLoc, 1, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(VertexInfo), (void *)offsetof(VertexInfo, height));
        glVertexAttribPointer (mixLoc,    1, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(VertexInfo), (void *)offsetof(VertexInfo, mix));
        glVertexAttribIPointer(texId1Loc, 1, GL_UNSIGNED_BYTE,          sizeof(VertexInfo), (void *)offsetof(VertexInfo, textureId1));
        glVertexAttribIPointer(texId2Loc, 1, GL_UNSIGNED_BYTE,          sizeof(VertexInfo), (void *)offsetof(VertexInfo, textureId2));
        
        // This doesn't need to be bound any more.
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        // Enable each of the 4 attributes.
        glEnableVertexAttribArray(heightLoc);
        glEnableVertexAttribArray(mixLoc);
        glEnableVertexAttribArray(texId1Loc);
        glEnableVertexAttribArray(texId2Loc);
    }
    
    void MainRoom::step(void) {
        
        // Clear colour buffer with colour, clear depth and stencil buffers.
        GLfloat colour[4] = { 0.56f, 0.92f, 1.0f, 1.0f };
        glClearBufferfv(GL_COLOR, 0, colour);
        glClearBufferfi(GL_DEPTH_STENCIL, 0, 1.0f, 0);
        
        // Use the shader program.
        glUseProgram(shaderProgram);
        
        // Update the look matrix.
        timeStep += 0.3f * getWindowBase()->elapsed();
        timeStep = fmod(timeStep, 2.0f * M_PI);
        modelViewTransform = glm::lookAt(
            glm::vec3(128 * cos(timeStep) + 128, 128 * -sin(timeStep) + 128, 50),
            glm::vec3(128,128,0), 
            glm::vec3(0, 0, 1)
        );
        
        /*modelViewTransform = glm::lookAt(
            glm::vec3(-3, -3, 3),
            glm::vec3(0,0,1.5), 
            glm::vec3(0, 0, 1)
        );*/
        
        // Smoothly increase world height from 0 to THEIGHTMAX, during the time between THEIGHT1 and THEIGHT2.
        #define THEIGHT1 10.0f
        #define THEIGHT2 15.0f
        #define THEIGHTMAX 48.0f
        if (totalTimeElapsed < THEIGHT1) {
            totalTimeElapsed += getWindowBase()->elapsed();
            glUniform1f(worldHeightUniform, 0.0f);
        } else {
            if (totalTimeElapsed < THEIGHT2) {
                glUniform1f(worldHeightUniform,
                    THEIGHTMAX - THEIGHTMAX * pow(2, -((totalTimeElapsed - THEIGHT1)/(THEIGHT2-THEIGHT1)) * 8.0f)
                );
                totalTimeElapsed += getWindowBase()->elapsed();
            } else {
                glUniform1f(worldHeightUniform, THEIGHTMAX);
            }
        }
        
        // Tell the shader about the new model view projection matrix.
        glm::mat4 mvpTransform = projectionTransform * modelViewTransform;
        glUniformMatrix4fv(mvpTransformMatrixUniform, 1, GL_FALSE, glm::value_ptr(mvpTransform));
        
        // Tell the shader about the current world size.
        glUniform1i(worldSizeUniform, worldSize);
        
        // Bind vertex array and index buffer.
        glBindVertexArray(vao);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertexIndexBuffer);
        
        // Draw vertexIndices vertices.
        glDrawElements(GL_TRIANGLES, vertexIndices, GL_UNSIGNED_INT, 0);
        
        // Unbind the buffer and vertex array.
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
        
        glutSwapBuffers();
    }
    
    void MainRoom::reshape(GLsizei width, GLsizei height) {
        glViewport(0.0f, 0.0f, width, height);
        GLfloat aspect = ((GLfloat)width) / ((GLfloat)height);
        // 50 degrees fovy
        projectionTransform = glm::perspective(0.872664626f, aspect, 0.1f, 1000.0f);
    }
    
    void MainRoom::end(void) {
    }
    void MainRoom::keyNormal(unsigned char key, int x, int y) {}
    void MainRoom::keyNormalRelease(unsigned char key, int x, int y) {}
    void MainRoom::keySpecial(int key, int x, int y) {}
    void MainRoom::keySpecialRelease(int key, int x, int y) {}
    void MainRoom::mouseEvent(int button, int state, int x, int y) {}
    void MainRoom::mouseMove(int x, int y) {}
    void MainRoom::mouseDrag(int x, int y) {}
    bool MainRoom::shouldDeleteOnRoomEnd(void) { return false; }

}
