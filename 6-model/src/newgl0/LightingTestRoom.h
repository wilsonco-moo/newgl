#ifndef NEWGL_LIGHTINGTESTROOM_H_
#define NEWGL_LIGHTINGTESTROOM_H_

#include <awe/lighting/LightingManager.h>
#include <awe/texture/TextureManager.h>
#include <wool/room/base/RoomBase.h>
#include <awe/world/PhysicsCamera.h>
#include <awe/model/ModelManager.h>
#include <awe/asset/AssetManager.h>
#include <awe/world/WorldManager.h>
#include <awe/instance/Instance.h>
#include <threading/ThreadQueue.h>
#include <awe/util/PhysicsWorld.h>
#include <awe/world/FreeCamera.h>
#include <awe/util/Types.h>
#include <GL/gl.h>

namespace newgl {

    class LightingTestRoom : public wool::RoomBase {
    private:
        unsigned int lastRemaining;
        GLuint shaderProgram,
               viewProjectionUniformLocation;
        threading::ThreadQueue threadQueue;
        awe::TextureManager textureManager;
        awe::ModelManager modelManager;
        awe::AssetManager assetManager;
        awe::LightingManager lightingManager;
        awe::PhysicsWorld physicsWorld;
        awe::PhysicsCamera camera;
        
        awe::WorldManager worldManager;
        
        GLfloat timeStep;
        
        // Picking up objects (grabbing instances).
        awe::Instance * grabbedInstance;
        awe::GLvec3 positionDiff;
        GLfloat startHDir, startVDir;
        bool rotating;
        
    public:
        LightingTestRoom(void);
        virtual ~LightingTestRoom(void);
    
    private:
        static void compileAndCheckShader(const std::string & name, GLuint shader);
        static void linkAndCheckProgram(const std::string & name, GLuint program);
        static GLuint compileShaderProgram(const std::string & name, const char * vertexShaderSource, const char * geometryShaderSource, const char * fragmentShaderSource);
        
        static GLuint buildDefaultShader(void);
        
        
        // ------------- Instance grabbing --------------
        void grabInstance(void);
        void startRotatingInstance(void);
        void stopRotatingInstance(void);
        void releaseInstance(void);
        void updateGrabbing(void);
        // ----------------------------------------------
        
    public:
        virtual void init(void) override;
        virtual void step(void) override;
        virtual void end(void) override;
        virtual void keyNormal(unsigned char key, int x, int y) override;
        virtual void keyNormalRelease(unsigned char key, int x, int y) override;
        virtual void keySpecial(int key, int x, int y) override;
        virtual void keySpecialRelease(int key, int x, int y) override;
        virtual void reshape(GLsizei width, GLsizei height) override;
        virtual void mouseEvent(int button, int state, int x, int y) override;
        virtual void mouseMove(int x, int y) override;
        virtual void mouseDrag(int x, int y) override;
        virtual bool shouldDeleteOnRoomEnd(void) override;
    };
}

#endif
