#ifndef NEWGL_INITROOM_H_
#define NEWGL_INITROOM_H_

#include <wool/room/base/RoomBase.h>

namespace newgl {

    class InitRoom : public wool::RoomBase {
    private:
        unsigned int frameCount;
        
    public:
        InitRoom(void);
        virtual ~InitRoom(void);

    public:
        virtual void init(void) override;
        virtual void step(void) override;
        virtual void end(void) override;
        virtual void keyNormal(unsigned char key, int x, int y) override;
        virtual void keyNormalRelease(unsigned char key, int x, int y) override;
        virtual void keySpecial(int key, int x, int y) override;
        virtual void keySpecialRelease(int key, int x, int y) override;
        virtual void reshape(GLsizei width, GLsizei height) override;
        virtual void mouseEvent(int button, int state, int x, int y) override;
        virtual void mouseMove(int x, int y) override;
        virtual void mouseDrag(int x, int y) override;
        virtual bool shouldDeleteOnRoomEnd(void) override;
    };
}

#endif
