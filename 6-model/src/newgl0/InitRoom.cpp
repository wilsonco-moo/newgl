#include <GL/gl3w.h> // Include first

#include "InitRoom.h"

#include <wool/window/base/WindowBase.h>
#include <GL/freeglut.h>
#include <iostream>
#include <cstdlib>
#include <GL/gl.h>

#include "MainRoom.h"
#include "InstanceTestRoom.h"
#include "LightingTestRoom.h"

//#define ENABLE_DEBUG_MESSAGES

#ifdef ENABLE_DEBUG_MESSAGES
    void APIENTRY
    MessageCallback( GLenum source,
                     GLenum type,
                     GLuint id,
                     GLenum severity,
                     GLsizei length,
                     const GLchar* message,
                     const void* userParam )
    {
      fprintf( stderr, "GL CALLBACK: %s type = 0x%x, severity = 0x%x, message = %s\n",
               ( type == GL_DEBUG_TYPE_ERROR ? "** GL ERROR **" : "" ),
                type, severity, message );
    }
#endif

namespace newgl {
    
    InitRoom::InitRoom(void) :
        frameCount(0) {
    }

    InitRoom::~InitRoom(void) {
    }
    
    void InitRoom::init(void) {
        if (gl3wInit()) {
            std::cerr << "Failed to initialize gl3w.\n";
            exit(EXIT_FAILURE);
        }
        if (!gl3wIsSupported(3, 3)) {
            std::cerr << "OpenGL 3.3 not supported\n";
            exit(EXIT_FAILURE);
        }
        glEnable(GL_DEPTH_TEST);
        
        glEnable(GL_CULL_FACE);
        glCullFace(GL_BACK);
        
        #ifdef ENABLE_DEBUG_MESSAGES
            // During init, enable debug output
            glEnable(GL_DEBUG_OUTPUT);
            glDebugMessageCallback(MessageCallback, 0);
        #endif
    }
    
    void InitRoom::step(void) {
        // Clear colour buffer with colour, clear depth and stencil buffers.
        GLfloat colour[4] = { 0.0f, 0.0f, 1.0f, 1.0f };
        glClearBufferfv(GL_COLOR, 0, colour);
        glClearBufferfi(GL_DEPTH_STENCIL, 0, 1.0f, 0);
        
        if (frameCount > 3) {
            getWindowBase()->changeRoom(new LightingTestRoom());
        } else {
            frameCount++;
        }
        
        glutSwapBuffers();
    }
    
    void InitRoom::reshape(GLsizei width, GLsizei height) {
        glViewport(0.0f, 0.0f, width, height);
    }
    
    void InitRoom::end(void) {}
    void InitRoom::keyNormal(unsigned char key, int x, int y) {}
    void InitRoom::keyNormalRelease(unsigned char key, int x, int y) {}
    void InitRoom::keySpecial(int key, int x, int y) {}
    void InitRoom::keySpecialRelease(int key, int x, int y) {}
    void InitRoom::mouseEvent(int button, int state, int x, int y) {}
    void InitRoom::mouseMove(int x, int y) {}
    void InitRoom::mouseDrag(int x, int y) {}
    bool InitRoom::shouldDeleteOnRoomEnd(void) { return true; }

}
