R"GLSL_END_TOKEN(#version 140

// Inputs (vertex attributes).
in vec3 inPosition;
in vec2 inUv;

// Outputs (to fragment shader).
out vec2 vfUv;

// Uniforms
uniform mat4 mvpTransformMatrix;

void main(void) {
    gl_Position = mvpTransformMatrix * vec4(inPosition, 1.0f);
    vfUv = inUv;
}
)GLSL_END_TOKEN"
