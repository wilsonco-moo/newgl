#ifndef NEWGL_INSTANCETESTROOM_H_
#define NEWGL_INSTANCETESTROOM_H_

#include <awe/texture/TextureManager.h>
#include <wool/room/base/RoomBase.h>
#include <awe/model/ModelManager.h>
#include <awe/asset/AssetManager.h>
#include <awe/instance/Instance.h>
#include <threading/ThreadQueue.h>
#include <awe/util/Types.h>
#include <GL/gl.h>

namespace newgl {

    class InstanceTestRoom : public wool::RoomBase {
    private:
        unsigned int lastRemaining;
        GLuint shaderProgram,
               viewProjectionUniformLocation;
        threading::ThreadQueue threadQueue;
        awe::TextureManager textureManager;
        awe::ModelManager modelManager;
        awe::AssetManager assetManager;
        
        awe::Instance someInstance, anotherInstance, middle1, middle2, computer;
        std::list<awe::Instance> tubes;
        GLfloat timeStep;
        
        // Projection and view transforms.
        awe::GLmat4 projectionTransform, viewTransform;
        
    public:
        InstanceTestRoom(void);
        virtual ~InstanceTestRoom(void);
    
    private:
        static void compileAndCheckShader(const std::string & name, GLuint shader);
        static void linkAndCheckProgram(const std::string & name, GLuint program);
        static GLuint compileShaderProgram(const std::string & name, const char * vertexShaderSource, const char * geometryShaderSource, const char * fragmentShaderSource);
        
        static GLuint buildDefaultShader(void);
        
    public:
        virtual void init(void) override;
        virtual void step(void) override;
        virtual void end(void) override;
        virtual void keyNormal(unsigned char key, int x, int y) override;
        virtual void keyNormalRelease(unsigned char key, int x, int y) override;
        virtual void keySpecial(int key, int x, int y) override;
        virtual void keySpecialRelease(int key, int x, int y) override;
        virtual void reshape(GLsizei width, GLsizei height) override;
        virtual void mouseEvent(int button, int state, int x, int y) override;
        virtual void mouseMove(int x, int y) override;
        virtual void mouseDrag(int x, int y) override;
        virtual bool shouldDeleteOnRoomEnd(void) override;
    };
}

#endif
