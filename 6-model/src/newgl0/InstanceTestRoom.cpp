#include <GL/gl3w.h> // Include first

#include "InstanceTestRoom.h"

#include <wool/window/base/WindowBase.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <awe/util/Types.h>
#include <awe/util/Util.h>
#include <GL/freeglut.h>
#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <cmath>
#include <list>

#include "MainRoom.h"

namespace newgl {
    
    InstanceTestRoom::InstanceTestRoom(void) :
        lastRemaining((unsigned int)-1),
        shaderProgram(buildDefaultShader()),
        viewProjectionUniformLocation(glGetUniformLocation(shaderProgram, "viewProjectionTransform")),
        threadQueue(),
        textureManager(&threadQueue, 512),
        modelManager(&threadQueue),
        assetManager(&threadQueue, &textureManager, &modelManager, shaderProgram),
        someInstance(&assetManager, NULL, "assets/pyramid/pyramid.xml", "someInstance", glm::translate(glm::scale(awe::GLmat4(1.0f), awe::GLvec3(2.0f, 2.0f, 2.0f)), awe::GLvec3(-1.5,0,0))),
        anotherInstance(&assetManager, NULL, "assets/pyramid/pyramid.xml", "anotherInstance", glm::translate(glm::scale(awe::GLmat4(1.0f), awe::GLvec3(2.0f, 2.0f, 2.0f)), awe::GLvec3(1.5,0,0))),
        middle1(&assetManager, NULL, "assets/pyramid/pyramid.xml", "middle1", glm::rotate(glm::translate(awe::GLmat4(1.0f), awe::GLvec3(0,1,0)),  (GLfloat)(M_PI*-0.5f), awe::GLvec3(1.0f, 0.0f, 0.0f))),
        middle2(&assetManager, NULL, "assets/pyramid/pyramid.xml", "middle2", glm::rotate(glm::translate(awe::GLmat4(1.0f), awe::GLvec3(0,-1,0)), (GLfloat)(M_PI*0.5f),  awe::GLvec3(1.0f, 0.0f, 0.0f))),
        computer(&assetManager, NULL, "assets/computer/computer.xml", "computer", glm::translate(glm::scale(awe::GLmat4(1.0f), awe::GLvec3(3.0f, 3.0f, 3.0f)), awe::GLvec3(1.8,0,0))),
        tubes(),
        timeStep(0.0f),
        projectionTransform(1.0f),
        viewTransform(1.0f) {
        
        #define TUBES_COUNT 48
        #define TUBE_STRIDE 1.8f
        #define TUBE_SCALE 0.4f
        #define TUBE_X1 ((GLfloat)(TUBES_COUNT - 1) * -0.5f * TUBE_STRIDE)
        
        for (unsigned int i = 0; i < TUBES_COUNT; i++) {
            tubes.emplace_back(
                &assetManager, (awe::PhysicsWorld *)NULL,
                "assets/tube/transformTube.xml",
                std::string("tube") + std::to_string(i),
                glm::translate(glm::scale(awe::GLmat4(1.0f), awe::GLvec3(TUBE_SCALE, TUBE_SCALE, TUBE_SCALE)), awe::GLvec3(TUBE_X1 + i*TUBE_STRIDE,3,0)));
        }
    }

    InstanceTestRoom::~InstanceTestRoom(void) {
        glDeleteShader(shaderProgram);
    }
    
    
    void InstanceTestRoom::compileAndCheckShader(const std::string & name, GLuint shader) {
        // Try to compile the shader.
        glCompileShader(shader);
        
        // If there is a log, print it on stderr.
        GLint len;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &len);
        if (len != 0) {
            std::string str(len, ' ');
            glGetShaderInfoLog(shader, len, NULL, &str[0]);
            std::cerr << str;
        }
        
        // If it didn't compile, complain.
        GLint isCompiled = 0;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
        if (isCompiled == GL_FALSE) {
            std::cerr << "ERROR: Failed to compile " << name << ".\n";
            exit(EXIT_FAILURE);
        }
    }
    
    void InstanceTestRoom::linkAndCheckProgram(const std::string & name, GLuint program) {
        // Try to link the shader.
        glLinkProgram(program);
        
        // If there is a log, print it on stderr.
        GLint len;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &len);
        if (len != 0) {
            std::string str(len, ' ');
            glGetProgramInfoLog(program, len, NULL, &str[0]);
            std::cerr << str;
        }
        
        // If it didn't link, complain.
        GLint isLinked = 0;
        glGetProgramiv(program, GL_LINK_STATUS, &isLinked);
        if (isLinked == GL_FALSE) {
            std::cerr << "ERROR: Failed to link " << name << ".\n";
            exit(EXIT_FAILURE);
        }
    }
    
    GLuint InstanceTestRoom::compileShaderProgram(const std::string & name, const char * vertexShaderSource, const char * geometryShaderSource, const char * fragmentShaderSource) {
        // Create and compile vertex shader.
        GLuint vertexShader = 0, geometryShader = 0, fragmentShader = 0;
        
        // Create and compile vertex shader.
        if (vertexShaderSource != NULL) {
            vertexShader = glCreateShader(GL_VERTEX_SHADER);
            glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
            compileAndCheckShader(name+": vertex shader", vertexShader);
        }
        
        // Create and compile geometry shader.
        if (geometryShaderSource != NULL) {
            geometryShader = glCreateShader(GL_GEOMETRY_SHADER);
            glShaderSource(geometryShader, 1, &geometryShaderSource, NULL);
            compileAndCheckShader(name+": geometry shader", geometryShader);
        }
        
        // Create and compile fragment shader.
        if (fragmentShaderSource != NULL) {
            fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
            glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
            compileAndCheckShader("fragment shader", fragmentShader);
        }
        
        // Create shader program, attach all shaders and link it.
        GLuint prog = glCreateProgram();
        if (vertexShaderSource != NULL)   glAttachShader(prog, vertexShader);
        if (geometryShaderSource != NULL) glAttachShader(prog, geometryShader);
        if (fragmentShaderSource != NULL) glAttachShader(prog, fragmentShader);
        linkAndCheckProgram("main shader program", prog);
        
        // Delete the compiled shaders: We don't need them any more since they
        // are part of the program.
        if (vertexShaderSource != NULL)   glDeleteShader(vertexShader);
        if (geometryShaderSource != NULL) glDeleteShader(geometryShader);
        if (fragmentShaderSource != NULL) glDeleteShader(fragmentShader);
        
        return prog;
    }
    
    GLuint InstanceTestRoom::buildDefaultShader(void) {
        return compileShaderProgram(
            "Main default shader",
            #include <awe/defaultShader/vertex.h>
            , NULL,
            #include <awe/defaultShader/fragment.h>
        );
    }
    
    void InstanceTestRoom::init(void) {
    }
    
    void InstanceTestRoom::step(void) {
        // Clear colour buffer with colour, clear depth and stencil buffers.
        GLfloat colour[4] = { 0.56f, 0.92f, 1.0f, 1.0f };
        glClearBufferfv(GL_COLOR, 0, colour);
        glClearBufferfi(GL_DEPTH_STENCIL, 0, 1.0f, 0);
        
        // Do load operations and print remaining.
        unsigned int remaining = 0;
        remaining += textureManager.step(3);
        remaining += modelManager.step();
        remaining += assetManager.step(3);
        if (remaining != lastRemaining) {
            lastRemaining = remaining;
            if (remaining != 0) {
                std::cout << "REMAINING: " << remaining << "\n";
            }
        }
        
        // Update the view transform matrix.
        //#define SPINNY
        timeStep += 0.8f * getWindowBase()->elapsed();
        timeStep = fmod(timeStep, 8.0f * M_PI);
        #ifdef SPINNY
            viewTransform = glm::lookAt(
                awe::GLvec3(10 * cos(timeStep), 10 * -sin(timeStep), 5),
                awe::GLvec3(0, 0, 0), 
                awe::GLvec3(0, 0, 1)
            );
        #else
            viewTransform = glm::lookAt(
                awe::GLvec3(0, -15, 10),
                awe::GLvec3(0, 0, 6), 
                awe::GLvec3(0, 0, 1)
            );
        #endif
        
        
        
        
        #define ROTATION_AMOUNT 0.4
        #define ROTATION_FULLCIRC (M_PI * 2.0f)
        #define ROTATION_OFF1 0
        #define ROTATION_OFF2 (M_PI)
        #define ROTATION_OFF3 (M_PI)
        #define ROTATION_OFF4 0
        #define SKELETON_DISTANCE (40.0f / 7.0f)
        
        #define TRANSF(id, off)                                                             \
            tube.getSkelNodes()[id].setBaseTransform(glm::rotate(                           \
                    glm::translate(                                                         \
                        glm::rotate(                                                        \
                            glm::mat4(1.0f),                                                \
                            (GLfloat)(sin(angle + off) * ROTATION_AMOUNT),                  \
                            glm::vec3(1, 0, 0)                                              \
                        ),                                                                  \
                        glm::vec3(0, SKELETON_DISTANCE, 0)                                  \
                    ),                                                                      \
                    (GLfloat)(sin(angle + off) * ROTATION_AMOUNT),                          \
                    glm::vec3(1, 0, 0)                                                      \
                ));
        
        unsigned int tubeId = 0;
        if (tubes.front().isLoaded()) {
            for (awe::Instance & tube : tubes) {
                GLfloat angle = timeStep + (((GLfloat)tubeId) / ((GLfloat)TUBES_COUNT)) * ROTATION_FULLCIRC;
                TRANSF(2, ROTATION_OFF1)
                TRANSF(3, ROTATION_OFF2)
                TRANSF(4, ROTATION_OFF3)
                TRANSF(5, ROTATION_OFF4)
                tube.getSkelNodes()[0].updateCalc();
                tubeId++;
            }
            tubes.front().updateAllInstances();
        }
        
        
        
        // Bind the shader program.
        glUseProgram(shaderProgram);
        
        // Update the view projection transform
        awe::GLmat4 viewProjectionTransform = projectionTransform * viewTransform;
        glUniformMatrix4fv(viewProjectionUniformLocation, 1, GL_FALSE, glm::value_ptr(viewProjectionTransform));
        
        // Draw all instances.
        assetManager.draw();
        
        // Unbind the shader program at the end.
        glUseProgram(0);
        
        glutSwapBuffers();
    }
    
    void InstanceTestRoom::reshape(GLsizei width, GLsizei height) {
        glViewport(0.0f, 0.0f, width, height);
        // 50 degrees fovy
        GLfloat aspect = ((GLfloat)width) / ((GLfloat)height);
        projectionTransform = glm::perspective(0.872664626f, aspect, 0.1f, 1000.0f);
    }
    
    void InstanceTestRoom::end(void) {}
    void InstanceTestRoom::keyNormal(unsigned char key, int x, int y) {}
    void InstanceTestRoom::keyNormalRelease(unsigned char key, int x, int y) {}
    void InstanceTestRoom::keySpecial(int key, int x, int y) {}
    void InstanceTestRoom::keySpecialRelease(int key, int x, int y) {}
    void InstanceTestRoom::mouseEvent(int button, int state, int x, int y) {}
    void InstanceTestRoom::mouseMove(int x, int y) {}
    void InstanceTestRoom::mouseDrag(int x, int y) {}
    bool InstanceTestRoom::shouldDeleteOnRoomEnd(void) { return true; }

}
