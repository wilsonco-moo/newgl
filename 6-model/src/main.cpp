
#include <GL/gl3w.h> // Include first

#include <GL/freeglut.h>
#include <iostream>
#include <cstdlib>
#include <vector>
#include <thread>
#include <chrono>

#include <wool/window/config/WindowConfig.h>
#include <wool/window/base/WindowBase.h>
#include <threading/ThreadQueue.h>

#include "newgl0/InitRoom.h"
//#include <awe/lighting/LightingManager.h>
#include <glm/gtc/matrix_transform.hpp>
#include <awe/texture/TextureManager.h>
#include <awe/model/ModelManager.h>
#include <awe/asset/AssetManager.h>
#include <awe/model/Model.h>
#include <awe/asset/Asset.h>
#include <awe/util/Types.h>
#include <awe/util/Util.h>

//#define MATRIX_TO_STRING

#ifdef MATRIX_TO_STRING
    void doMatrixToString(void);
    void matrixMaths(void);
#endif

int main(int argc, char * argv[]) {
    
    #ifdef MATRIX_TO_STRING
        doMatrixToString();
        return 0;
    #endif
    /*
    {
        threading::ThreadQueue queue;
        awe::ModelManager modelManager(&queue);
        awe::TextureManager textureManager(&queue, 512);
        awe::AssetManager assetManager(&queue, &textureManager, &modelManager);
        
        awe::Asset * asset1 = assetManager.requestAsset("assets/pyramid/pyramid.xml"),
                   * asset2 = assetManager.requestAsset("assets/pyramid/transformPyramid.xml");
        
        assetManager.waitForLoad();
        
        std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    }
    */
    srand(time(NULL));
    {
        newgl::InitRoom * room = new newgl::InitRoom();
        wool::WindowConfig config;
        config.windowTitle = (char *)"OpenGL!";
        config.enableWindozeVsync = true;
        config.width = 640;
        config.height = 480;
        config.displayMode = GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH;
        wool::WindowBase windowBase(room, config);
        windowBase.enableFrameRateLogging = true;
        windowBase.start(&argc, argv);
    }
    std::cout << "Program done.\n";
    return EXIT_SUCCESS;
}


#ifdef MATRIX_TO_STRING

    void doMatrixToString(void) {
        //awe::LightingManager lighting(0, awe::GLuvec3(16,16,16), awe::GLvec3(-4,-4,-4), awe::GLvec3(4,4,4));
        matrixMaths();
        return;
        glm::mat4 matrix = glm::mat4(1.0f);
            glm::rotate(
                glm::mat4(1.0f),
                (GLfloat)M_PI,
                glm::vec3(1, 0, 0)
            );
        std::cout << awe::Util::mat4ToString(matrix, false);
        std::cout << "\n\n";
    }

    awe::GLvec3 getAxis(void) {
        while(true) {
            std::cout << "Enter axis (x/y/z): ";
            std::string axis;
            std::getline(std::cin, axis);
            if (axis == "x") {
                return awe::GLvec3(1,0,0);
            } else if (axis == "y") {
                return awe::GLvec3(0,1,0);
            } else if (axis == "z") {
                return awe::GLvec3(0,0,1);
            }
        }
    }

    GLfloat getAmount(const std::string & name) {
        std::cout << "Enter amount " << name << ": ";
        std::string amountStr;
        std::getline(std::cin, amountStr);
        GLfloat amount = std::strtof(amountStr.c_str(), NULL);
        return amount;
    }

    void matrixMaths(void) {
        awe::GLmat4 matrix(1.0f);
        while(true) {
            std::cout << "Enter: r (rotate), s (scale), g (grab/move), or d (done): ";
            std::string op;
            std::getline(std::cin, op);
            if (op == "r") {
                awe::GLvec3 vec = getAxis();
                GLfloat amount = getAmount("to rotate (degrees)");
                matrix = glm::rotate(awe::GLmat4(1.0f), glm::radians(amount), vec) * matrix;
            } else if (op == "s") {
                awe::GLvec3 vec = getAxis();
                GLfloat amount = getAmount("to scale (times)");
                if (vec.x == 0) vec.x = 1.0f; else vec.x = amount;
                if (vec.y == 0) vec.y = 1.0f; else vec.y = amount;
                if (vec.z == 0) vec.z = 1.0f; else vec.z = amount;
                matrix = glm::scale(awe::GLmat4(1.0f), vec) * matrix;
            } else if (op == "g") {
                awe::GLvec3 vec = getAxis();
                GLfloat amount = getAmount("to move by (world units)");
                matrix = glm::translate(awe::GLmat4(1.0f), vec * amount) * matrix;
            } else if (op == "d") {
                std::cout <<
                    "\n----------- Inline hex (no precision loss) -----------\n\n" <<
                    awe::Util::mat4ToString(matrix, true, false) <<
                    "\n\n--------------- Inline decimal -----------------------\n\n" <<
                    awe::Util::mat4ToString(matrix, false, false) <<
                    "\n\n------------- Grid (looks nice and readable) ---------\n\n" <<
                    awe::Util::mat4ToString(matrix, false) <<
                    "\n\n------------------------------------------------------\n\n";
                matrix = awe::GLmat4(1.0f);
            }
        }
    }

#endif
