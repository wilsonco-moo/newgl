# Modern OpenGL demo projects

This repository contains a series of small projects, which I have
developed while experimenting with modern OpenGL. To learn how to do this, I
used the [OpenGL SuperBible](http://www.openglsuperbible.com) (seventh
edition).

Note that the later demo projects (at the bottom) are more interesting.
All of these demo projects use the [Waf build system](https://waf.io), just
like [Rasc](https://gitlab.com/wilsonco-moo/rasc). To compile them, see the
build instructions for [Rasc](https://gitlab.com/wilsonco-moo/rasc). The
asset/model/texture features developed in the last demo project were spun off
into a separate project: [AWE](https://gitlab.com/wilsonco-moo/awe).
The last demo also uses physics from the
[ReactPhysics3D library](https://github.com/DanielChappuis/reactphysics3d).

Note that when running the OpenGL 3.1 based demos on second gen intel
integrated graphics, additional options are required since the linux
drivers don't fully support OpenGL 3.1. They can be launched like this:
`MESA_GL_VERSION_OVERRIDE=3.1 MESA_GLSL_VERSION_OVERRIDE=140 ./debugLaunch`.
I developed most of these demos using OpenGL 3.1 rather than the latest
version for better hardware compatibility, (so I can run them on my Thinkpad
from 2012).


## 0: Spinning pyramid (OpenGL 4.4)

A spinning pyramid, coloured with shaders. I suppose that drawing a single 3d
shape like this is the OpenGL equivalent of a hello world program.

![Image](development/screenshots/0-shaderSpinnyPyramid.png)


## 1: Textured spinning pyrarmid (OpenGL 4.4)

Here I replaced one of the sides of the pyramid with a texture. As it turns out,
mapping a texture to screen space coordinates on a moving 3d shape, produces
quite an interesting visual effect.

![Image](development/screenshots/1-shaderTextureSpinnyPyramid.png)


## 2: Heightmap v1 (OpenGL 4.4)

This is my first attempt at a heightmap system, which allows two textures
to be mixed together. It seemed like a good idea (at the time) to only store
the heights and colours at each position once. I did this using the
`relativeOffset` vertex array option to provide vertex array attributes for the
other three points, then drawing each point using `GL_POINTS`. A geometry shader
then turns each point into 4 triangle strip vertices. This sounded like a good
idea, but it turned out to be *much* more complicated than necessary, and the
maximum `relativeOffset` severely limited the heightmap size.

![Image](development/screenshots/2-heightmap.png)


## 3: Heightmap v2 (OpenGL 3.1)

My second attempt at a heightmap system, which allows many different textures to
be used at once, but only two textures mixed together in any one place. I took
a more conventional approach this time: a vertex buffer stores a height, two
texture IDs and a mix, for each point. Then, an index buffer arranges these
points into a series of triangles, which are drawn using `glDrawElements`. The
vertex shader works out x,y coordinates from the vertex ID and the world size,
and the fragment shader mixes the two textures.

To make loading and storage much more convenient, a
[single image](https://gitlab.com/wilsonco-moo/newgl/-/raw/master/3-heightmap/assets/map.png)
stores the heightmap. Each of the four channels represents something different:
red is height, green is mix, blue is texture ID 1, alpha is texture ID 2.
This single image is built from a
[traditional heightmap](https://gitlab.com/wilsonco-moo/newgl/-/raw/master/3-heightmap/developmentAssets/heightmap.png),
a texture representing
[terrain type](https://gitlab.com/wilsonco-moo/newgl/-/raw/master/3-heightmap/developmentAssets/colours.png),
and a
[definition](https://gitlab.com/wilsonco-moo/newgl/-/blob/master/3-heightmap/developmentAssets/colourDefs)
of each terrain type and it's *"blurryness"*, beforehand by a
[simple java program](https://gitlab.com/wilsonco-moo/newgl/-/blob/master/3-heightmap/developmentAssets/ColourConv.java).

![Image](development/screenshots/3-heightmap.png)


## 4: Transformation demo (OpenGL 3.1)

Here, I worked out a simple way of deforming a model using a skeleton. At the
start, each skeleton node has an initial position, and each vertex works out the
closest two skeleton nodes and distances to them.

The skeleton nodes are then given transformations. The vertex shader linearly
interpolates the transformation matrices of the those closest two nodes, based
on the relative initial distances, then applies this to the original vertex
position. This results in smooth deformation of the model.

![Image](development/screenshots/4-transform.png)


## 5: Texture loading (OpenGL 3.1)

This demo was mainly written as a stress test of a texture loading system.
It repeatedly requests that textures are loaded and unloaded, in order to test
that the system can keep up.

![Image](development/screenshots/5-texturing.png)


## 6: Models, physics, lighting and first person camera

This demo allows a player to walk around in a room, and interact with physics
objects around them. This can be done either by pushing them (walking into
them), or lifting/throwing them. The model, asset and texture loading, as well
lighting was spun off into a separate library:
[AWE](https://gitlab.com/wilsonco-moo/awe).

Physics is provided using the
[ReactPhysics3D library](https://github.com/DanielChappuis/reactphysics3d).

Note that credit goes to Andrew Ackerley for the design of models and textures
used in this demo. All models were created using
[Blender](https://www.blender.org).

AWE provides a simple 3d engine, designed for enclosed room-like scenes, and
has the following features:
 * Models are loaded from obj files by a simple obj file loader.
 * Textures and object space normal maps are loaded from png files by the
   texture loader from the previous demo.
 * Assets (specified by xml files) are a combination of a model, a texture
   and an object space normal map. Assets are either dynamic (can be moved)
   or static (like the floor and the walls). Multiple assets are able to share
   the same model or texture, without the model/texture being duplicated.
 * Instances can be created and placed in the scene. Each instance references
   a particular asset, allowing for convenient instanced rendering.
 * Lights are represented in a grid: Up to 256 lights are possible, with up to
   4 light sources having influence over each cell of a 32x32x4 grid.
   This seems to work very well for an enclosed room environment like this.
 * A world (specified by an xml file), specifies the location of each instance
   and light source.
 * New assets and worlds can be dynamically loaded and unloaded.

![Image](development/screenshots/6-model.png)
