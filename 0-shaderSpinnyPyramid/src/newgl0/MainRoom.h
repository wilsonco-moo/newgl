#ifndef NEWGL_MAINROOM_H_
#define NEWGL_MAINROOM_H_

#include <wool/room/base/RoomBase.h>
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include <GL/gl.h>
#include <array>

namespace newgl {

    class MainRoom : public wool::RoomBase {
    private:
        const static std::array<glm::vec3, 6 * 3> vertices;
        
        // The vertex array object for feeding data to the shader, from the buffer.
        GLuint vao;
        
        // The buffer in which we can store the vertices.
        GLuint buffer;
        
        // The projection matrix, (view space to clip space).
        glm::mat4 projectionTransform;
        
        // The model-view matrix, (world (model) space to view space).
        glm::mat4 modelViewTransform;
        
        // Our shader program. This is set up in compileShaders.
        GLuint shaderProgram;
        
        // The projection and model view transform uniform locations.
        GLuint projectionTransformUniform,
               modelViewTransformUniform;
        
        GLfloat timeStep;
               
    public:
        MainRoom(void);
        virtual ~MainRoom(void);
    private:
        void initGl3w(void);
        void initVertices(void);
        static void compileAndCheckShader(GLuint shader);
        void compileShaders(void);
        void setupUniforms(void);

    public:
        virtual void init(void) override;
        virtual void step(void) override;
        virtual void end(void) override;
        virtual void keyNormal(unsigned char key, int x, int y) override;
        virtual void keyNormalRelease(unsigned char key, int x, int y) override;
        virtual void keySpecial(int key, int x, int y) override;
        virtual void keySpecialRelease(int key, int x, int y) override;
        virtual void reshape(GLsizei width, GLsizei height) override;
        virtual void mouseEvent(int button, int state, int x, int y) override;
        virtual void mouseMove(int x, int y) override;
        virtual void mouseDrag(int x, int y) override;
        virtual bool shouldDeleteOnRoomEnd(void) override;

    };
}

#endif
