R"GLSL_END_TOKEN(#version 440 core

// Read positions as vec3's.
layout (location = 0) in vec3 inPosition;
// Write colours as vec4's.
out vec4 vfColour;

// The transformations for vertices.
uniform mat4 projectionTransform;
uniform mat4 modelViewTransform;

void main(void) {
    // Convert the position to a vec4, multiply by model view transform,
    // then by projection transform.
    gl_Position =
        projectionTransform *
        modelViewTransform *
        vec4(inPosition, 1.0f);
    
    
    // Set the out colour. Mix based on vertex id.
    int vId = gl_VertexID % 3;
    vfColour = vec4(
        vId == 0 ? 1.0f : 0.0f, // R
        vId == 1 ? 1.0f : 0.0f, // G
        vId == 2 ? 1.0f : 0.0f, // B
        1.0f                    // A
    );
}
)GLSL_END_TOKEN"
