R"GLSL_END_TOKEN(#version 440 core

// Input and output.
in vec4 vfColour;
out vec4 outColour;

void main(void) {
    // Multiply the red, green and blue channels by 10, mod them.
    vec3 col = vec3(vfColour.r, vfColour.g, vfColour.b);
    col = mod(col * 10, 1.0f);
    outColour = vec4(col, 1.0f);
}
)GLSL_END_TOKEN"
