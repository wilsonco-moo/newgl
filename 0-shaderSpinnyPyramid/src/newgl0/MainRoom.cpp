#include <GL/gl3w.h> // Include first

#include "MainRoom.h"

#include <wool/window/base/WindowBase.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/freeglut.h>
#include <iostream>
#include <cstdlib>
#include <GL/gl.h>

namespace newgl {
    
    const std::array<glm::vec3, 6 * 3> MainRoom::vertices = {
        glm::vec3(1,-1,0), glm::vec3(-1,-1,0), glm::vec3(1,1,0),   // base 1
        glm::vec3(1,1,0),  glm::vec3(-1,-1,0), glm::vec3(-1,1,0),  // base 2
        
        glm::vec3(0,0,1),  glm::vec3(1,-1,0),  glm::vec3(1,1,0),   // right
        glm::vec3(0,0,1),  glm::vec3(-1,-1,0), glm::vec3(1,-1,0),  // top
        glm::vec3(0,0,1),  glm::vec3(-1,1,0),  glm::vec3(-1,-1,0), // left
        glm::vec3(0,0,1),  glm::vec3(1,1,0),   glm::vec3(-1,1,0),  // bottom
    };
    
    MainRoom::MainRoom(void) :
        vao(0),
        buffer(0),
        projectionTransform(),
        modelViewTransform(),
        shaderProgram(0),
        projectionTransformUniform(0),
        modelViewTransformUniform(0),
        timeStep(0.0f) {
    }

    MainRoom::~MainRoom(void) {
        
    }

    void MainRoom::init(void) {
        initGl3w();
        initVertices();
        compileShaders();
        setupUniforms();
        // Misc initialisation
        glEnable(GL_DEPTH_TEST);
        
    }
    
    void MainRoom::initGl3w(void) {
        if (gl3wInit()) {
            std::cerr << "Failed to initialize gl3w.\n";
            exit(EXIT_FAILURE);
        }
        if (!gl3wIsSupported(4, 4)) {
            std::cerr << "OpenGL 4.4 not supported\n";
            exit(EXIT_FAILURE);
        }
    }    
    
    void MainRoom::initVertices(void) {
        
        // Create ourself a buffer.
        glCreateBuffers(1, &buffer);
        // Allocate it memory, give it the vertices as initial data. Use no flags: We don't ever need to change this.
        glNamedBufferStorage(buffer, vertices.size() * sizeof(glm::vec3), &vertices[0], 0);
        
        // Create a vertex array object.
        glCreateVertexArrays(1, &vao);
        
        // Create attribute zero in this vertex array.
        // Atribute zero is from the buffer bound at binding index zero.
        glVertexArrayAttribBinding(vao, 0, 0);
        // Attribute zero is made of 3 float values, without normalisation, with zero offset.
        glVertexArrayAttribFormat(vao, 0, 3, GL_FLOAT, GL_FALSE, 0);
        
        // Bind the buffer to binding index zero within the vertex array object.
        glVertexArrayVertexBuffer(vao, 0, buffer, 0, sizeof(glm::vec3));
        
        // Enable attribute zero (enable automatic fetching).
        glEnableVertexArrayAttrib(vao, 0);
        
        // Bind the vertex array and buffer.
        glBindVertexArray(vao);
        glBindBuffer(GL_ARRAY_BUFFER, buffer);
    }
    
    void MainRoom::compileAndCheckShader(GLuint shader) {
        // Try to compile the shader.
        glCompileShader(shader);
        
        // If there is a log, print it on stderr.
        GLint len;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &len);
        if (len != 0) {
            std::string str(len, ' ');
            glGetShaderInfoLog(shader, len, NULL, &str[0]);
            std::cerr << str;
        }
        
        // If it didn't compile, complain.
        GLint isCompiled = 0;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
        if (isCompiled == GL_FALSE) {
            std::cerr << "ERROR: Failed to compile shader.\n";
            exit(EXIT_FAILURE);
        }
    }
    
    void MainRoom::compileShaders(void) {
        
        // String literals for shader sources.
        const char * vertexShaderSource = 
        #include "shaders/vertex.h"
        ;
        const char * fragmentShaderSource = 
        #include "shaders/fragment.h"
        ;
        
        // Create and compile vertex shader.
        GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
        compileAndCheckShader(vertexShader);
        
        // Create and compile fragment shader.
        GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
        compileAndCheckShader(fragmentShader);
        
        // Create shader program, attach both shaders and link it.
        shaderProgram = glCreateProgram();
        glAttachShader(shaderProgram, vertexShader);
        glAttachShader(shaderProgram, fragmentShader);
        glLinkProgram(shaderProgram);
        
        // Delete the compiled shaders: We don't need them any more since they
        // are part of the program.
        glDeleteShader(vertexShader);
        glDeleteShader(fragmentShader);
    }
    
    void MainRoom::setupUniforms(void) {
        projectionTransformUniform = glGetUniformLocation(shaderProgram, "projectionTransform");
        modelViewTransformUniform = glGetUniformLocation(shaderProgram, "modelViewTransform");
    }
    
    void MainRoom::step(void) {
        
        // Clear colour buffer with colour, clear depth and stencil buffers.
        GLfloat colour[4] = { 0.0f, 1.0f, 1.0f, 1.0f };
        glClearBufferfv(GL_COLOR, 0, colour);
        glClearBufferfi(GL_DEPTH_STENCIL, 0, 1.0f, 0);
        
        // Use the shader program.
        glUseProgram(shaderProgram);
        
        // Update the look matrix.
        timeStep += getWindowBase()->elapsed() * 0.5f;
        timeStep = fmod(timeStep, 2.0f * M_PI);
        modelViewTransform = glm::lookAt(glm::vec3(3 * cos(timeStep), 3 * -sin(timeStep), 2), glm::vec3(0,0,0), glm::vec3(0, 0, 1));
        
        // Set the projection and model view transformation matrices (uniforms).
        glUniformMatrix4fv(projectionTransformUniform, 1, GL_FALSE, glm::value_ptr(projectionTransform));
        glUniformMatrix4fv(modelViewTransformUniform, 1, GL_FALSE, glm::value_ptr(modelViewTransform));
        
        // Draw the appropriate number of triangles. This will load the vertices
        // from the buffer automatically.
        glDrawArrays(GL_TRIANGLES, 0, vertices.size());
        
        glutSwapBuffers();
    }
    
    void MainRoom::reshape(GLsizei width, GLsizei height) {
        glViewport(0.0f, 0.0f, width, height);
        GLfloat aspect = ((GLfloat)width) / ((GLfloat)height);
        // 50 degrees fovy
        projectionTransform = glm::perspective(0.872664626f, aspect, 0.1f, 1000.0f);
    }
    
    void MainRoom::end(void) {
    }
    void MainRoom::keyNormal(unsigned char key, int x, int y) {}
    void MainRoom::keyNormalRelease(unsigned char key, int x, int y) {}
    void MainRoom::keySpecial(int key, int x, int y) {}
    void MainRoom::keySpecialRelease(int key, int x, int y) {}
    void MainRoom::mouseEvent(int button, int state, int x, int y) {}
    void MainRoom::mouseMove(int x, int y) {}
    void MainRoom::mouseDrag(int x, int y) {}
    bool MainRoom::shouldDeleteOnRoomEnd(void) { return false; }

}
